aws.cli: command ?=
aws.cli: tty ?=
aws.cli:
	@docker run --rm $(tty) --env "AWS_ACCESS_KEY_ID=${AWS_ACCESS_KEY_ID}" --env "AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY}" --env "AWS_DEFAULT_REGION=${AWS_DEFAULT_REGION}" infrastructureascode/aws-cli ash $(command)

login.ecr: ## Login to the AWS registry
	@$(shell make aws.cli tty="" command="aws ecr get-login --no-include-email")

create.service: cluster ?= AXP-PROD
create.service: environment ?= UAT
create.service: service ?= AXP-CMS-$(environment)
create.service: version ?= $(environment)
create.service: ## 🆕  Update service in ECS, default AXP-CMS-Staging
	$(info 🆕  Create service $(service) in ECS cluster $(cluster)) 
	#cd etc/ecs && terraform init && terraform apply	

remove.service: cluster ?= AXP-PROD
remove.service: environment ?= UAT
remove.service: service ?= AXP-CMS-$(environment)
remove.service: ## ❌  Remove service in ECS, default AXP-CMS-Staging
	$(info ❌  Destroy service $(service) in ECS cluster $(cluster) ...) 
	#cd etc/ecs && terraform init && terraform destroy	

remove.image: repo ?= axp/cms/app
remove.image: version ?= $(shell git describe --always --abbrev=0 --tags)
remove.image: ## Remove Docker image from registy
	aws ecr batch-delete-image --repository-name $(repo) --image-ids imageTag=$(version)		

update.service: cluster ?= AXP-PROD
update.service: environment ?= QA
update.service: service ?= AXP-CMS-$(environment)
update.service: task ?= AXP-CMS-$(environment)
update.service: ## 🚀  Update service in ECS, default AXP-CMS-QA
	$(info 🚀  Updating service $(service) in ECS cluster $(cluster) ...) 
	aws ecs update-service --cluster $(cluster) --service $(service) --task-definition $(task) --force-new-deployment --query 'service.{status:status,pendingCount:pendingCount,desiredCount:desiredCount,runningCount:runningCount,serviceName:serviceName,taskDefinition:taskDefinition}'		

revert.service: cluster ?= AXP-PROD
revert.service: environment ?= QA
revert.service: service ?= AXP-CMS-$(environment)
revert.service: ## ⏪  Update service in ECS, default AXP-CMS-Staging
	$(info ⏪  Reverting service $(service) in ECS cluster $(cluster) ...) 
	$(info Not implemented yet) 	

deploy.service: cluster ?= AXP-PROD
deploy.service: environment ?= Staging
deploy.service: service ?= AXP-CMS-API-$(environment)
deploy.service: repo ?= axp/cms/app
deploy.service: version ?= $(shell git describe --always --abbrev=0 --tags)
deploy.service: ## 🚀 Deploy service in ECS, default AXP-CMS-Staging
	$(info 🚀 Deploying $(version) of $(service) to cluster $(cluster))		
	ecs-deploy -c $(cluster) -n $(service) -i $(repo):$(version) --skip-deployments-check --max-definitions 3
	
deploy.service.multitask: cluster ?= AXP-PROD
deploy.service.multitask: environment ?= Staging
deploy.service.multitask: service ?= AXP-CMS-$(environment)
deploy.service.multitask: version ?= $(shell git describe --always --abbrev=0 --tags)
deploy.service.multitask: ## 🚀 Deploy service in ECS, default AXP-CMS-Staging
	$(info 🚀 Deploying $(version) of $(service) to cluster $(cluster))		
	ecs-deploy -c $(cluster) -n $(service) -to $(version) -i ignore --skip-deployments-check --max-definitions 3