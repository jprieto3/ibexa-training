-- Adminer 4.8.0 MySQL 5.5.5-10.3.29-MariaDB-1:10.3.29+maria~focal dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP DATABASE IF EXISTS `ezplatform`;
CREATE DATABASE `ezplatform` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `ezplatform`;

SET NAMES utf8mb4;

DROP TABLE IF EXISTS `ezbinaryfile`;
CREATE TABLE `ezbinaryfile` (
  `contentobject_attribute_id` int(11) NOT NULL DEFAULT 0,
  `version` int(11) NOT NULL DEFAULT 0,
  `download_count` int(11) NOT NULL DEFAULT 0,
  `filename` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `mime_type` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `original_filename` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`contentobject_attribute_id`,`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


DROP TABLE IF EXISTS `ezcobj_state`;
CREATE TABLE `ezcobj_state` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `default_language_id` bigint(20) NOT NULL DEFAULT 0,
  `group_id` int(11) NOT NULL DEFAULT 0,
  `identifier` varchar(45) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `language_mask` bigint(20) NOT NULL DEFAULT 0,
  `priority` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ezcobj_state_identifier` (`group_id`,`identifier`),
  KEY `ezcobj_state_priority` (`priority`),
  KEY `ezcobj_state_lmask` (`language_mask`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

INSERT INTO `ezcobj_state` (`id`, `default_language_id`, `group_id`, `identifier`, `language_mask`, `priority`) VALUES
(1,	2,	2,	'not_locked',	3,	0),
(2,	2,	2,	'locked',	3,	1);

DROP TABLE IF EXISTS `ezcobj_state_group`;
CREATE TABLE `ezcobj_state_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `default_language_id` bigint(20) NOT NULL DEFAULT 0,
  `identifier` varchar(45) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `language_mask` bigint(20) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ezcobj_state_group_identifier` (`identifier`),
  KEY `ezcobj_state_group_lmask` (`language_mask`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

INSERT INTO `ezcobj_state_group` (`id`, `default_language_id`, `identifier`, `language_mask`) VALUES
(2,	2,	'ez_lock',	3);

DROP TABLE IF EXISTS `ezcobj_state_group_language`;
CREATE TABLE `ezcobj_state_group_language` (
  `contentobject_state_group_id` int(11) NOT NULL DEFAULT 0,
  `real_language_id` bigint(20) NOT NULL DEFAULT 0,
  `description` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `language_id` bigint(20) NOT NULL DEFAULT 0,
  `name` varchar(45) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`contentobject_state_group_id`,`real_language_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

INSERT INTO `ezcobj_state_group_language` (`contentobject_state_group_id`, `real_language_id`, `description`, `language_id`, `name`) VALUES
(2,	2,	'',	3,	'Lock');

DROP TABLE IF EXISTS `ezcobj_state_language`;
CREATE TABLE `ezcobj_state_language` (
  `contentobject_state_id` int(11) NOT NULL DEFAULT 0,
  `language_id` bigint(20) NOT NULL DEFAULT 0,
  `description` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `name` varchar(45) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`contentobject_state_id`,`language_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

INSERT INTO `ezcobj_state_language` (`contentobject_state_id`, `language_id`, `description`, `name`) VALUES
(1,	3,	'',	'Not locked'),
(2,	3,	'',	'Locked');

DROP TABLE IF EXISTS `ezcobj_state_link`;
CREATE TABLE `ezcobj_state_link` (
  `contentobject_id` int(11) NOT NULL DEFAULT 0,
  `contentobject_state_id` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`contentobject_id`,`contentobject_state_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

INSERT INTO `ezcobj_state_link` (`contentobject_id`, `contentobject_state_id`) VALUES
(1,	1),
(4,	1),
(10,	1),
(11,	1),
(12,	1),
(13,	1),
(14,	1),
(41,	1),
(42,	1),
(49,	1),
(50,	1),
(51,	1),
(52,	1),
(53,	1),
(54,	1),
(55,	1);

DROP TABLE IF EXISTS `ezcontentbrowsebookmark`;
CREATE TABLE `ezcontentbrowsebookmark` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `node_id` int(11) NOT NULL DEFAULT 0,
  `user_id` int(11) NOT NULL DEFAULT 0,
  `name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `ezcontentbrowsebookmark_location` (`node_id`),
  KEY `ezcontentbrowsebookmark_user` (`user_id`),
  KEY `ezcontentbrowsebookmark_user_location` (`user_id`,`node_id`),
  CONSTRAINT `ezcontentbrowsebookmark_location_fk` FOREIGN KEY (`node_id`) REFERENCES `ezcontentobject_tree` (`node_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `ezcontentbrowsebookmark_user_fk` FOREIGN KEY (`user_id`) REFERENCES `ezuser` (`contentobject_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


DROP TABLE IF EXISTS `ezcontentclass`;
CREATE TABLE `ezcontentclass` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `version` int(11) NOT NULL DEFAULT 0,
  `always_available` int(11) NOT NULL DEFAULT 0,
  `contentobject_name` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `created` int(11) NOT NULL DEFAULT 0,
  `creator_id` int(11) NOT NULL DEFAULT 0,
  `identifier` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `initial_language_id` bigint(20) NOT NULL DEFAULT 0,
  `is_container` int(11) NOT NULL DEFAULT 0,
  `language_mask` bigint(20) NOT NULL DEFAULT 0,
  `modified` int(11) NOT NULL DEFAULT 0,
  `modifier_id` int(11) NOT NULL DEFAULT 0,
  `remote_id` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `serialized_description_list` longtext COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `serialized_name_list` longtext COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `sort_field` int(11) NOT NULL DEFAULT 1,
  `sort_order` int(11) NOT NULL DEFAULT 1,
  `url_alias_name` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  PRIMARY KEY (`id`,`version`),
  KEY `ezcontentclass_version` (`version`),
  KEY `ezcontentclass_identifier` (`identifier`,`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

INSERT INTO `ezcontentclass` (`id`, `version`, `always_available`, `contentobject_name`, `created`, `creator_id`, `identifier`, `initial_language_id`, `is_container`, `language_mask`, `modified`, `modifier_id`, `remote_id`, `serialized_description_list`, `serialized_name_list`, `sort_field`, `sort_order`, `url_alias_name`) VALUES
(1,	0,	1,	'<short_name|name>',	1024392098,	14,	'folder',	2,	1,	2,	1448831672,	14,	'a3d405b81be900468eb153d774f4f0d2',	'a:0:{}',	'a:1:{s:6:\"eng-GB\";s:6:\"Folder\";}',	1,	1,	NULL),
(2,	0,	0,	'<short_title|title>',	1024392098,	14,	'article',	2,	1,	3,	1082454989,	14,	'c15b600eb9198b1924063b5a68758232',	NULL,	'a:2:{s:6:\"eng-GB\";s:7:\"Article\";s:16:\"always-available\";s:6:\"eng-GB\";}',	1,	1,	NULL),
(3,	0,	1,	'<name>',	1024392098,	14,	'user_group',	2,	1,	3,	1048494743,	14,	'25b4268cdcd01921b808a0d854b877ef',	NULL,	'a:2:{s:6:\"eng-GB\";s:10:\"User group\";s:16:\"always-available\";s:6:\"eng-GB\";}',	1,	1,	NULL),
(4,	0,	1,	'<first_name> <last_name>',	1024392098,	14,	'user',	2,	0,	3,	1082018364,	14,	'40faa822edc579b02c25f6bb7beec3ad',	NULL,	'a:2:{s:6:\"eng-GB\";s:4:\"User\";s:16:\"always-available\";s:6:\"eng-GB\";}',	1,	1,	NULL),
(5,	0,	1,	'<name>',	1031484992,	14,	'image',	2,	0,	3,	1048494784,	14,	'f6df12aa74e36230eb675f364fccd25a',	NULL,	'a:2:{s:6:\"eng-GB\";s:5:\"Image\";s:16:\"always-available\";s:6:\"eng-GB\";}',	1,	1,	NULL),
(12,	0,	1,	'<name>',	1052385472,	14,	'file',	2,	0,	3,	1052385669,	14,	'637d58bfddf164627bdfd265733280a0',	NULL,	'a:2:{s:6:\"eng-GB\";s:4:\"File\";s:16:\"always-available\";s:6:\"eng-GB\";}',	1,	1,	NULL),
(42,	0,	1,	'<name>',	1435924826,	14,	'landing_page',	2,	1,	2,	1435924826,	14,	'60c03e9758465eb69d56b3afb6adf18e',	'a:1:{s:6:\"eng-GB\";s:0:\"\";}',	'a:1:{s:6:\"eng-GB\";s:12:\"Landing page\";}',	2,	0,	''),
(43,	0,	1,	'<title>',	1537166773,	14,	'form',	2,	0,	2,	1537166834,	14,	'6f7f21df775a33c1e4bbc76b48c38476',	'a:0:{}',	'a:1:{s:6:\"eng-GB\";s:4:\"Form\";}',	2,	0,	'');

DROP TABLE IF EXISTS `ezcontentclassgroup`;
CREATE TABLE `ezcontentclassgroup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created` int(11) NOT NULL DEFAULT 0,
  `creator_id` int(11) NOT NULL DEFAULT 0,
  `modified` int(11) NOT NULL DEFAULT 0,
  `modifier_id` int(11) NOT NULL DEFAULT 0,
  `name` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

INSERT INTO `ezcontentclassgroup` (`id`, `created`, `creator_id`, `modified`, `modifier_id`, `name`) VALUES
(1,	1031216928,	14,	1033922106,	14,	'Content'),
(2,	1031216941,	14,	1033922113,	14,	'Users'),
(3,	1032009743,	14,	1033922120,	14,	'Media');

DROP TABLE IF EXISTS `ezcontentclass_attribute`;
CREATE TABLE `ezcontentclass_attribute` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `version` int(11) NOT NULL DEFAULT 0,
  `can_translate` int(11) DEFAULT 1,
  `category` varchar(25) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `contentclass_id` int(11) NOT NULL DEFAULT 0,
  `data_float1` double DEFAULT NULL,
  `data_float2` double DEFAULT NULL,
  `data_float3` double DEFAULT NULL,
  `data_float4` double DEFAULT NULL,
  `data_int1` int(11) DEFAULT NULL,
  `data_int2` int(11) DEFAULT NULL,
  `data_int3` int(11) DEFAULT NULL,
  `data_int4` int(11) DEFAULT NULL,
  `data_text1` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `data_text2` varchar(50) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `data_text3` varchar(50) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `data_text4` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `data_text5` longtext COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `data_type_string` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `identifier` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `is_information_collector` int(11) NOT NULL DEFAULT 0,
  `is_required` int(11) NOT NULL DEFAULT 0,
  `is_searchable` int(11) NOT NULL DEFAULT 0,
  `is_thumbnail` tinyint(1) NOT NULL DEFAULT 0,
  `placement` int(11) NOT NULL DEFAULT 0,
  `serialized_data_text` longtext COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `serialized_description_list` longtext COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `serialized_name_list` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  PRIMARY KEY (`id`,`version`),
  KEY `ezcontentclass_attr_ccid` (`contentclass_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

INSERT INTO `ezcontentclass_attribute` (`id`, `version`, `can_translate`, `category`, `contentclass_id`, `data_float1`, `data_float2`, `data_float3`, `data_float4`, `data_int1`, `data_int2`, `data_int3`, `data_int4`, `data_text1`, `data_text2`, `data_text3`, `data_text4`, `data_text5`, `data_type_string`, `identifier`, `is_information_collector`, `is_required`, `is_searchable`, `is_thumbnail`, `placement`, `serialized_data_text`, `serialized_description_list`, `serialized_name_list`) VALUES
(1,	0,	1,	'',	2,	0,	0,	0,	0,	255,	0,	0,	0,	'New article',	'',	'',	'',	'',	'ezstring',	'title',	0,	1,	1,	0,	1,	NULL,	NULL,	'a:2:{s:6:\"eng-GB\";s:5:\"Title\";s:16:\"always-available\";s:6:\"eng-GB\";}'),
(4,	0,	1,	'',	1,	NULL,	NULL,	NULL,	NULL,	255,	0,	NULL,	NULL,	'Folder',	NULL,	NULL,	NULL,	NULL,	'ezstring',	'name',	0,	1,	1,	0,	1,	'N;',	'a:0:{}',	'a:1:{s:6:\"eng-GB\";s:4:\"Name\";}'),
(6,	0,	1,	'',	3,	0,	0,	0,	0,	255,	0,	0,	0,	'',	'',	'',	'',	NULL,	'ezstring',	'name',	0,	1,	1,	0,	1,	NULL,	NULL,	'a:2:{s:6:\"eng-GB\";s:4:\"Name\";s:16:\"always-available\";s:6:\"eng-GB\";}'),
(7,	0,	1,	'',	3,	0,	0,	0,	0,	255,	0,	0,	0,	'',	'',	'',	'',	NULL,	'ezstring',	'description',	0,	0,	1,	0,	2,	NULL,	NULL,	'a:2:{s:6:\"eng-GB\";s:11:\"Description\";s:16:\"always-available\";s:6:\"eng-GB\";}'),
(8,	0,	1,	'',	4,	0,	0,	0,	0,	255,	0,	0,	0,	'',	'',	'',	'',	'',	'ezstring',	'first_name',	0,	1,	1,	0,	1,	NULL,	NULL,	'a:2:{s:6:\"eng-GB\";s:10:\"First name\";s:16:\"always-available\";s:6:\"eng-GB\";}'),
(9,	0,	1,	'',	4,	0,	0,	0,	0,	255,	0,	0,	0,	'',	'',	'',	'',	'',	'ezstring',	'last_name',	0,	1,	1,	0,	2,	NULL,	NULL,	'a:2:{s:6:\"eng-GB\";s:9:\"Last name\";s:16:\"always-available\";s:6:\"eng-GB\";}'),
(12,	0,	0,	'',	4,	0,	0,	0,	0,	7,	10,	0,	0,	'',	'^[^@]+$',	'',	'',	'',	'ezuser',	'user_account',	0,	1,	0,	0,	3,	NULL,	NULL,	'a:2:{s:6:\"eng-GB\";s:12:\"User account\";s:16:\"always-available\";s:6:\"eng-GB\";}'),
(116,	0,	1,	'',	5,	0,	0,	0,	0,	150,	0,	0,	0,	'',	'',	'',	'',	NULL,	'ezstring',	'name',	0,	1,	1,	0,	1,	NULL,	NULL,	'a:2:{s:6:\"eng-GB\";s:4:\"Name\";s:16:\"always-available\";s:6:\"eng-GB\";}'),
(117,	0,	1,	'',	5,	0,	0,	0,	0,	10,	0,	0,	0,	'',	'',	'',	'',	NULL,	'ezrichtext',	'caption',	0,	0,	1,	0,	2,	NULL,	NULL,	'a:2:{s:6:\"eng-GB\";s:7:\"Caption\";s:16:\"always-available\";s:6:\"eng-GB\";}'),
(118,	0,	1,	'',	5,	0,	0,	0,	0,	10,	0,	0,	0,	'',	'',	'',	'',	NULL,	'ezimage',	'image',	0,	0,	0,	0,	3,	NULL,	NULL,	'a:2:{s:6:\"eng-GB\";s:5:\"Image\";s:16:\"always-available\";s:6:\"eng-GB\";}'),
(119,	0,	1,	'',	1,	NULL,	NULL,	NULL,	NULL,	5,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	'ezrichtext',	'short_description',	0,	0,	1,	0,	3,	'N;',	'a:0:{}',	'a:1:{s:6:\"eng-GB\";s:17:\"Short description\";}'),
(120,	0,	1,	'',	2,	0,	0,	0,	0,	10,	0,	0,	0,	'',	'',	'',	'',	'',	'ezrichtext',	'intro',	0,	1,	1,	0,	4,	NULL,	NULL,	'a:2:{s:6:\"eng-GB\";s:5:\"Intro\";s:16:\"always-available\";s:6:\"eng-GB\";}'),
(121,	0,	1,	'',	2,	0,	0,	0,	0,	20,	0,	0,	0,	'',	'',	'',	'',	'',	'ezrichtext',	'body',	0,	0,	1,	0,	5,	NULL,	NULL,	'a:2:{s:6:\"eng-GB\";s:4:\"Body\";s:16:\"always-available\";s:6:\"eng-GB\";}'),
(123,	0,	0,	'',	2,	0,	0,	0,	0,	0,	0,	0,	0,	'',	'',	'',	'',	'',	'ezboolean',	'enable_comments',	0,	0,	0,	0,	6,	NULL,	NULL,	'a:2:{s:6:\"eng-GB\";s:15:\"Enable comments\";s:16:\"always-available\";s:6:\"eng-GB\";}'),
(146,	0,	1,	'',	12,	0,	0,	0,	0,	0,	0,	0,	0,	'New file',	'',	'',	'',	NULL,	'ezstring',	'name',	0,	1,	1,	0,	1,	NULL,	NULL,	'a:2:{s:6:\"eng-GB\";s:4:\"Name\";s:16:\"always-available\";s:6:\"eng-GB\";}'),
(147,	0,	1,	'',	12,	0,	0,	0,	0,	10,	0,	0,	0,	'',	'',	'',	'',	NULL,	'ezrichtext',	'description',	0,	0,	1,	0,	2,	NULL,	NULL,	'a:2:{s:6:\"eng-GB\";s:11:\"Description\";s:16:\"always-available\";s:6:\"eng-GB\";}'),
(148,	0,	1,	'',	12,	0,	0,	0,	0,	0,	0,	0,	0,	'',	'',	'',	'',	NULL,	'ezbinaryfile',	'file',	0,	1,	0,	0,	3,	NULL,	NULL,	'a:2:{s:6:\"eng-GB\";s:4:\"File\";s:16:\"always-available\";s:6:\"eng-GB\";}'),
(152,	0,	1,	'',	2,	0,	0,	0,	0,	255,	0,	0,	0,	'',	'',	'',	'',	'',	'ezstring',	'short_title',	0,	0,	1,	0,	2,	NULL,	NULL,	'a:2:{s:6:\"eng-GB\";s:11:\"Short title\";s:16:\"always-available\";s:6:\"eng-GB\";}'),
(153,	0,	1,	'',	2,	0,	0,	0,	0,	1,	0,	0,	0,	'',	'',	'',	'',	'',	'ezauthor',	'author',	0,	0,	0,	0,	3,	NULL,	NULL,	'a:2:{s:6:\"eng-GB\";s:6:\"Author\";s:16:\"always-available\";s:6:\"eng-GB\";}'),
(154,	0,	1,	'',	2,	0,	0,	0,	0,	0,	0,	0,	0,	'',	'',	'',	'',	'',	'ezobjectrelation',	'image',	0,	0,	1,	0,	7,	NULL,	NULL,	'a:2:{s:6:\"eng-GB\";s:5:\"Image\";s:16:\"always-available\";s:6:\"eng-GB\";}'),
(155,	0,	1,	'',	1,	NULL,	NULL,	NULL,	NULL,	100,	0,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	'ezstring',	'short_name',	0,	0,	1,	0,	2,	'N;',	'a:0:{}',	'a:1:{s:6:\"eng-GB\";s:10:\"Short name\";}'),
(156,	0,	1,	'',	1,	NULL,	NULL,	NULL,	NULL,	20,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	'ezrichtext',	'description',	0,	0,	1,	0,	4,	'N;',	'a:0:{}',	'a:1:{s:6:\"eng-GB\";s:11:\"Description\";}'),
(179,	0,	1,	'',	4,	0,	0,	0,	0,	10,	0,	0,	0,	'',	'',	'',	'',	'',	'eztext',	'signature',	0,	0,	1,	0,	4,	NULL,	NULL,	'a:2:{s:6:\"eng-GB\";s:9:\"Signature\";s:16:\"always-available\";s:6:\"eng-GB\";}'),
(180,	0,	1,	'',	4,	0,	0,	0,	0,	10,	0,	0,	0,	'',	'',	'',	'',	'',	'ezimage',	'image',	0,	0,	0,	0,	5,	NULL,	NULL,	'a:2:{s:6:\"eng-GB\";s:5:\"Image\";s:16:\"always-available\";s:6:\"eng-GB\";}'),
(185,	0,	1,	'content',	42,	NULL,	NULL,	NULL,	NULL,	0,	0,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	'ezstring',	'name',	0,	1,	1,	0,	10,	'N;',	'a:1:{s:6:\"eng-GB\";s:5:\"Title\";}',	'a:1:{s:6:\"eng-GB\";s:5:\"Title\";}'),
(186,	0,	1,	'content',	42,	NULL,	NULL,	NULL,	NULL,	0,	0,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	'ezstring',	'description',	0,	1,	1,	0,	20,	'N;',	'a:1:{s:6:\"eng-GB\";s:24:\"Landing page description\";}',	'a:1:{s:6:\"eng-GB\";s:11:\"Description\";}'),
(187,	0,	1,	'content',	42,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	'ezlandingpage',	'page',	0,	0,	0,	0,	30,	'N;',	'a:1:{s:6:\"eng-GB\";s:12:\"Landing page\";}',	'a:1:{s:6:\"eng-GB\";s:12:\"Landing page\";}'),
(188,	0,	1,	'content',	43,	NULL,	NULL,	NULL,	NULL,	0,	0,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	'ezstring',	'title',	0,	1,	1,	0,	1,	'N;',	'a:0:{}',	'a:1:{s:6:\"eng-GB\";s:5:\"Title\";}'),
(189,	0,	1,	'content',	43,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	'ezform',	'form',	0,	0,	0,	0,	2,	'N;',	'a:0:{}',	'a:1:{s:6:\"eng-GB\";s:4:\"Form\";}');

DROP TABLE IF EXISTS `ezcontentclass_attribute_ml`;
CREATE TABLE `ezcontentclass_attribute_ml` (
  `contentclass_attribute_id` int(11) NOT NULL,
  `version` int(11) NOT NULL,
  `language_id` bigint(20) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `data_text` text COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `data_json` text COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  PRIMARY KEY (`contentclass_attribute_id`,`version`,`language_id`),
  KEY `ezcontentclass_attribute_ml_lang_fk` (`language_id`),
  CONSTRAINT `ezcontentclass_attribute_ml_lang_fk` FOREIGN KEY (`language_id`) REFERENCES `ezcontent_language` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


DROP TABLE IF EXISTS `ezcontentclass_classgroup`;
CREATE TABLE `ezcontentclass_classgroup` (
  `contentclass_id` int(11) NOT NULL DEFAULT 0,
  `contentclass_version` int(11) NOT NULL DEFAULT 0,
  `group_id` int(11) NOT NULL DEFAULT 0,
  `group_name` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  PRIMARY KEY (`contentclass_id`,`contentclass_version`,`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

INSERT INTO `ezcontentclass_classgroup` (`contentclass_id`, `contentclass_version`, `group_id`, `group_name`) VALUES
(1,	0,	1,	'Content'),
(2,	0,	1,	'Content'),
(3,	0,	2,	'Users'),
(4,	0,	2,	'Users'),
(5,	0,	3,	'Media'),
(12,	0,	3,	'Media'),
(42,	0,	1,	'Content'),
(43,	0,	1,	'Content');

DROP TABLE IF EXISTS `ezcontentclass_name`;
CREATE TABLE `ezcontentclass_name` (
  `contentclass_id` int(11) NOT NULL DEFAULT 0,
  `contentclass_version` int(11) NOT NULL DEFAULT 0,
  `language_id` bigint(20) NOT NULL DEFAULT 0,
  `language_locale` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`contentclass_id`,`contentclass_version`,`language_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

INSERT INTO `ezcontentclass_name` (`contentclass_id`, `contentclass_version`, `language_id`, `language_locale`, `name`) VALUES
(1,	0,	2,	'eng-GB',	'Folder'),
(2,	0,	3,	'eng-GB',	'Article'),
(3,	0,	3,	'eng-GB',	'User group'),
(4,	0,	3,	'eng-GB',	'User'),
(5,	0,	3,	'eng-GB',	'Image'),
(12,	0,	3,	'eng-GB',	'File'),
(42,	0,	2,	'eng-GB',	'Landing page'),
(43,	0,	2,	'eng-GB',	'Form');

DROP TABLE IF EXISTS `ezcontentobject`;
CREATE TABLE `ezcontentobject` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contentclass_id` int(11) NOT NULL DEFAULT 0,
  `current_version` int(11) DEFAULT NULL,
  `initial_language_id` bigint(20) NOT NULL DEFAULT 0,
  `language_mask` bigint(20) NOT NULL DEFAULT 0,
  `modified` int(11) NOT NULL DEFAULT 0,
  `name` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `owner_id` int(11) NOT NULL DEFAULT 0,
  `published` int(11) NOT NULL DEFAULT 0,
  `remote_id` varchar(100) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `section_id` int(11) NOT NULL DEFAULT 0,
  `status` int(11) DEFAULT 0,
  `is_hidden` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ezcontentobject_remote_id` (`remote_id`),
  KEY `ezcontentobject_classid` (`contentclass_id`),
  KEY `ezcontentobject_lmask` (`language_mask`),
  KEY `ezcontentobject_pub` (`published`),
  KEY `ezcontentobject_section` (`section_id`),
  KEY `ezcontentobject_currentversion` (`current_version`),
  KEY `ezcontentobject_owner` (`owner_id`),
  KEY `ezcontentobject_status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

INSERT INTO `ezcontentobject` (`id`, `contentclass_id`, `current_version`, `initial_language_id`, `language_mask`, `modified`, `name`, `owner_id`, `published`, `remote_id`, `section_id`, `status`, `is_hidden`) VALUES
(1,	1,	9,	2,	3,	1448889046,	'Ibexa Platform',	14,	1448889046,	'9459d3c29e15006e45197295722c7ade',	1,	1,	0),
(4,	3,	1,	2,	3,	1033917596,	'Users',	14,	1033917596,	'f5c88a2209584891056f987fd965b0ba',	2,	1,	0),
(10,	4,	2,	2,	3,	1072180405,	'Anonymous User',	14,	1033920665,	'faaeb9be3bd98ed09f606fc16d144eca',	2,	1,	0),
(11,	3,	1,	2,	3,	1033920746,	'Guest accounts',	14,	1033920746,	'5f7f0bdb3381d6a461d8c29ff53d908f',	2,	1,	0),
(12,	3,	1,	2,	3,	1033920775,	'Administrator users',	14,	1033920775,	'9b47a45624b023b1a76c73b74d704acf',	2,	1,	0),
(13,	3,	1,	2,	3,	1033920794,	'Editors',	14,	1033920794,	'3c160cca19fb135f83bd02d911f04db2',	2,	1,	0),
(14,	4,	3,	2,	3,	1301062024,	'Administrator User',	14,	1033920830,	'1bb4fe25487f05527efa8bfd394cecc7',	2,	1,	0),
(41,	1,	1,	2,	3,	1060695457,	'Media',	14,	1060695457,	'a6e35cbcb7cd6ae4b691f3eee30cd262',	3,	1,	0),
(42,	3,	1,	2,	3,	1072180330,	'Anonymous Users',	14,	1072180330,	'15b256dbea2ae72418ff5facc999e8f9',	2,	1,	0),
(49,	1,	1,	2,	3,	1080220197,	'Images',	14,	1080220197,	'e7ff633c6b8e0fd3531e74c6e712bead',	3,	1,	0),
(50,	1,	1,	2,	3,	1080220220,	'Files',	14,	1080220220,	'732a5acd01b51a6fe6eab448ad4138a9',	3,	1,	0),
(51,	1,	1,	2,	3,	1080220233,	'Multimedia',	14,	1080220233,	'09082deb98662a104f325aaa8c4933d3',	3,	1,	0),
(52,	42,	1,	2,	3,	1442481743,	'Ibexa Digital Experience Platform',	14,	1442481743,	'34720ff636e1d4ce512f762dc638e4ac',	1,	1,	0),
(53,	1,	1,	2,	3,	1486473151,	'Form Uploads',	14,	1486473151,	'6797ab09a3e84316f09c4ccabce90e2d',	3,	1,	0),
(54,	1,	1,	2,	2,	1537166893,	'Forms',	14,	1537166893,	'9e863fbb0fb835ce050032b4f00de61d',	6,	1,	0),
(55,	1,	1,	2,	3,	1586855342,	'Site skeletons',	14,	1586855342,	'1ac4a4b7108e607682beaba14ba860c5',	7,	1,	0);

DROP TABLE IF EXISTS `ezcontentobject_attribute`;
CREATE TABLE `ezcontentobject_attribute` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `version` int(11) NOT NULL DEFAULT 0,
  `attribute_original_id` int(11) DEFAULT 0,
  `contentclassattribute_id` int(11) NOT NULL DEFAULT 0,
  `contentobject_id` int(11) NOT NULL DEFAULT 0,
  `data_float` double DEFAULT NULL,
  `data_int` int(11) DEFAULT NULL,
  `data_text` longtext COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `data_type_string` varchar(50) COLLATE utf8mb4_unicode_520_ci DEFAULT '',
  `language_code` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `language_id` bigint(20) NOT NULL DEFAULT 0,
  `sort_key_int` int(11) NOT NULL DEFAULT 0,
  `sort_key_string` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`,`version`),
  KEY `ezcontentobject_attribute_co_id_ver_lang_code` (`contentobject_id`,`version`,`language_code`),
  KEY `ezcontentobject_classattr_id` (`contentclassattribute_id`),
  KEY `sort_key_string` (`sort_key_string`(191)),
  KEY `ezcontentobject_attribute_language_code` (`language_code`),
  KEY `sort_key_int` (`sort_key_int`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

INSERT INTO `ezcontentobject_attribute` (`id`, `version`, `attribute_original_id`, `contentclassattribute_id`, `contentobject_id`, `data_float`, `data_int`, `data_text`, `data_type_string`, `language_code`, `language_id`, `sort_key_int`, `sort_key_string`) VALUES
(1,	9,	0,	4,	1,	NULL,	NULL,	'Ibexa Platform',	'ezstring',	'eng-GB',	3,	0,	'ibexa platform'),
(2,	9,	0,	119,	1,	NULL,	NULL,	'<?xml version=\"1.0\" encoding=\"UTF-8\"?><section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:ezxhtml=\"http://ez.no/xmlns/ezpublish/docbook/xhtml\" xmlns:ezcustom=\"http://ez.no/xmlns/ezpublish/docbook/custom\" version=\"5.0-variant ezpublish-1.0\"><para><emphasis role=\"strong\">You are now ready to start your project.</emphasis></para></section>',	'ezrichtext',	'eng-GB',	3,	0,	''),
(7,	1,	0,	7,	4,	NULL,	NULL,	'Main group',	'ezstring',	'eng-GB',	3,	0,	''),
(8,	1,	0,	6,	4,	NULL,	NULL,	'Users',	'ezstring',	'eng-GB',	3,	0,	''),
(19,	2,	0,	8,	10,	0,	0,	'Anonymous',	'ezstring',	'eng-GB',	3,	0,	'anonymous'),
(20,	2,	0,	9,	10,	0,	0,	'User',	'ezstring',	'eng-GB',	3,	0,	'user'),
(21,	2,	0,	12,	10,	0,	0,	'',	'ezuser',	'eng-GB',	3,	0,	''),
(22,	1,	0,	6,	11,	0,	0,	'Guest accounts',	'ezstring',	'eng-GB',	3,	0,	''),
(23,	1,	0,	7,	11,	0,	0,	'',	'ezstring',	'eng-GB',	3,	0,	''),
(24,	1,	0,	6,	12,	0,	0,	'Administrator users',	'ezstring',	'eng-GB',	3,	0,	''),
(25,	1,	0,	7,	12,	0,	0,	'',	'ezstring',	'eng-GB',	3,	0,	''),
(26,	1,	0,	6,	13,	0,	0,	'Editors',	'ezstring',	'eng-GB',	3,	0,	''),
(27,	1,	0,	7,	13,	0,	0,	'',	'ezstring',	'eng-GB',	3,	0,	''),
(28,	3,	0,	8,	14,	0,	0,	'Administrator',	'ezstring',	'eng-GB',	3,	0,	'administrator'),
(29,	3,	0,	9,	14,	0,	0,	'User',	'ezstring',	'eng-GB',	3,	0,	'user'),
(30,	3,	30,	12,	14,	0,	0,	'',	'ezuser',	'eng-GB',	3,	0,	''),
(98,	1,	0,	4,	41,	0,	0,	'Media',	'ezstring',	'eng-GB',	3,	0,	''),
(99,	1,	0,	119,	41,	0,	1045487555,	'<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:ezxhtml=\"http://ez.no/xmlns/ezpublish/docbook/xhtml\" xmlns:ezcustom=\"http://ez.no/xmlns/ezpublish/docbook/custom\" version=\"5.0-variant ezpublish-1.0\"/>\n',	'ezrichtext',	'eng-GB',	3,	0,	''),
(100,	1,	0,	6,	42,	0,	0,	'Anonymous Users',	'ezstring',	'eng-GB',	3,	0,	'anonymous users'),
(101,	1,	0,	7,	42,	0,	0,	'User group for the anonymous user',	'ezstring',	'eng-GB',	3,	0,	'user group for the anonymous user'),
(102,	9,	0,	155,	1,	NULL,	NULL,	'Ibexa Platform',	'ezstring',	'eng-GB',	3,	0,	'ibexa platform'),
(103,	1,	0,	155,	41,	0,	0,	'',	'ezstring',	'eng-GB',	3,	0,	''),
(104,	9,	0,	156,	1,	NULL,	NULL,	'<?xml version=\"1.0\" encoding=\"UTF-8\"?><section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:ezxhtml=\"http://ez.no/xmlns/ezpublish/docbook/xhtml\" xmlns:ezcustom=\"http://ez.no/xmlns/ezpublish/docbook/custom\" version=\"5.0-variant ezpublish-1.0\"><para>This is the clean installation coming with Ibexa Platform.</para><para>It\'s a bare-bones setup of the Platform, an excellent foundation to build upon if you want to start your project from scratch.</para></section>',	'ezrichtext',	'eng-GB',	3,	0,	''),
(105,	1,	0,	156,	41,	0,	1045487555,	'<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:ezxhtml=\"http://ez.no/xmlns/ezpublish/docbook/xhtml\" xmlns:ezcustom=\"http://ez.no/xmlns/ezpublish/docbook/custom\" version=\"5.0-variant ezpublish-1.0\"/>\n',	'ezrichtext',	'eng-GB',	3,	0,	''),
(142,	1,	0,	4,	49,	0,	0,	'Images',	'ezstring',	'eng-GB',	3,	0,	'images'),
(143,	1,	0,	155,	49,	0,	0,	'',	'ezstring',	'eng-GB',	3,	0,	''),
(144,	1,	0,	119,	49,	0,	1045487555,	'<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:ezxhtml=\"http://ez.no/xmlns/ezpublish/docbook/xhtml\" xmlns:ezcustom=\"http://ez.no/xmlns/ezpublish/docbook/custom\" version=\"5.0-variant ezpublish-1.0\"/>\n',	'ezrichtext',	'eng-GB',	3,	0,	''),
(145,	1,	0,	156,	49,	0,	1045487555,	'<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:ezxhtml=\"http://ez.no/xmlns/ezpublish/docbook/xhtml\" xmlns:ezcustom=\"http://ez.no/xmlns/ezpublish/docbook/custom\" version=\"5.0-variant ezpublish-1.0\"/>\n',	'ezrichtext',	'eng-GB',	3,	0,	''),
(147,	1,	0,	4,	50,	0,	0,	'Files',	'ezstring',	'eng-GB',	3,	0,	'files'),
(148,	1,	0,	155,	50,	0,	0,	'',	'ezstring',	'eng-GB',	3,	0,	''),
(149,	1,	0,	119,	50,	0,	1045487555,	'<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:ezxhtml=\"http://ez.no/xmlns/ezpublish/docbook/xhtml\" xmlns:ezcustom=\"http://ez.no/xmlns/ezpublish/docbook/custom\" version=\"5.0-variant ezpublish-1.0\"/>\n',	'ezrichtext',	'eng-GB',	3,	0,	''),
(150,	1,	0,	156,	50,	0,	1045487555,	'<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:ezxhtml=\"http://ez.no/xmlns/ezpublish/docbook/xhtml\" xmlns:ezcustom=\"http://ez.no/xmlns/ezpublish/docbook/custom\" version=\"5.0-variant ezpublish-1.0\"/>\n',	'ezrichtext',	'eng-GB',	3,	0,	''),
(152,	1,	0,	4,	51,	0,	0,	'Multimedia',	'ezstring',	'eng-GB',	3,	0,	'multimedia'),
(153,	1,	0,	155,	51,	0,	0,	'',	'ezstring',	'eng-GB',	3,	0,	''),
(154,	1,	0,	119,	51,	0,	1045487555,	'<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:ezxhtml=\"http://ez.no/xmlns/ezpublish/docbook/xhtml\" xmlns:ezcustom=\"http://ez.no/xmlns/ezpublish/docbook/custom\" version=\"5.0-variant ezpublish-1.0\"/>\n',	'ezrichtext',	'eng-GB',	3,	0,	''),
(155,	1,	0,	156,	51,	0,	1045487555,	'<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:ezxhtml=\"http://ez.no/xmlns/ezpublish/docbook/xhtml\" xmlns:ezcustom=\"http://ez.no/xmlns/ezpublish/docbook/custom\" version=\"5.0-variant ezpublish-1.0\"/>\n',	'ezrichtext',	'eng-GB',	3,	0,	''),
(177,	2,	0,	179,	10,	0,	0,	'',	'eztext',	'eng-GB',	3,	0,	''),
(178,	3,	0,	179,	14,	0,	0,	'',	'eztext',	'eng-GB',	3,	0,	''),
(179,	2,	0,	180,	10,	0,	0,	'',	'ezimage',	'eng-GB',	3,	0,	''),
(180,	3,	0,	180,	14,	0,	0,	'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<ezimage serial_number=\"1\" is_valid=\"\" filename=\"\" suffix=\"\" basename=\"\" dirpath=\"\" url=\"\" original_filename=\"\" mime_type=\"\" width=\"\" height=\"\" alternative_text=\"\" alias_key=\"1293033771\" timestamp=\"1301057722\"><original attribute_id=\"180\" attribute_version=\"3\" attribute_language=\"eng-GB\"/></ezimage>\n',	'ezimage',	'eng-GB',	3,	0,	''),
(242,	1,	0,	185,	52,	NULL,	NULL,	'Ibexa Digital Experience Platform',	'ezstring',	'eng-GB',	3,	0,	'ibexa digital experience platform'),
(243,	1,	0,	186,	52,	NULL,	NULL,	'You are now ready to start your project.',	'ezstring',	'eng-GB',	3,	0,	'you are now ready to start your project.'),
(244,	1,	0,	187,	52,	NULL,	NULL,	NULL,	'ezlandingpage',	'eng-GB',	3,	0,	'ibexa digital experience platform'),
(245,	1,	0,	4,	53,	NULL,	NULL,	'Form Uploads',	'ezstring',	'eng-GB',	3,	0,	'form uploads'),
(246,	1,	0,	155,	53,	NULL,	NULL,	'form uploads',	'ezstring',	'eng-GB',	3,	0,	'form uploads'),
(247,	1,	0,	119,	53,	NULL,	NULL,	'<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:ezxhtml=\"http://ez.no/xmlns/ezpublish/docbook/xhtml\" xmlns:ezcustom=\"http://ez.no/xmlns/ezpublish/docbook/custom\" version=\"5.0-variant ezpublish-1.0\"><para>Folder for file uploads</para></section>\n',	'ezrichtext',	'eng-GB',	3,	0,	''),
(248,	1,	0,	156,	53,	NULL,	NULL,	'<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" version=\"5.0-variant ezpublish-1.0\"/>\n',	'ezrichtext',	'eng-GB',	3,	0,	''),
(249,	1,	0,	4,	54,	NULL,	NULL,	'Forms',	'ezstring',	'eng-GB',	2,	0,	'forms'),
(250,	1,	0,	155,	54,	NULL,	NULL,	NULL,	'ezstring',	'eng-GB',	2,	0,	''),
(251,	1,	0,	119,	54,	NULL,	NULL,	'<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" version=\"5.0-variant ezpublish-1.0\"/>\n',	'ezrichtext',	'eng-GB',	2,	0,	''),
(252,	1,	0,	156,	54,	NULL,	NULL,	'<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" version=\"5.0-variant ezpublish-1.0\"/>\n',	'ezrichtext',	'eng-GB',	2,	0,	''),
(253,	1,	0,	4,	55,	NULL,	NULL,	'Site skeletons',	'ezstring',	'eng-GB',	3,	0,	'site skeletons'),
(254,	1,	0,	155,	55,	NULL,	NULL,	NULL,	'ezstring',	'eng-GB',	3,	0,	''),
(255,	1,	0,	119,	55,	NULL,	NULL,	'<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" version=\"5.0-variant ezpublish-1.0\"/>\n',	'ezrichtext',	'eng-GB',	3,	0,	''),
(256,	1,	0,	156,	55,	NULL,	NULL,	'<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" version=\"5.0-variant ezpublish-1.0\"/>\n',	'ezrichtext',	'eng-GB',	3,	0,	'');

DROP TABLE IF EXISTS `ezcontentobject_link`;
CREATE TABLE `ezcontentobject_link` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contentclassattribute_id` int(11) NOT NULL DEFAULT 0,
  `from_contentobject_id` int(11) NOT NULL DEFAULT 0,
  `from_contentobject_version` int(11) NOT NULL DEFAULT 0,
  `relation_type` int(11) NOT NULL DEFAULT 1,
  `to_contentobject_id` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `ezco_link_to_co_id` (`to_contentobject_id`),
  KEY `ezco_link_from` (`from_contentobject_id`,`from_contentobject_version`,`contentclassattribute_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


DROP TABLE IF EXISTS `ezcontentobject_name`;
CREATE TABLE `ezcontentobject_name` (
  `contentobject_id` int(11) NOT NULL DEFAULT 0,
  `content_version` int(11) NOT NULL DEFAULT 0,
  `content_translation` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `language_id` bigint(20) NOT NULL DEFAULT 0,
  `name` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `real_translation` varchar(20) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  PRIMARY KEY (`contentobject_id`,`content_version`,`content_translation`),
  KEY `ezcontentobject_name_lang_id` (`language_id`),
  KEY `ezcontentobject_name_cov_id` (`content_version`),
  KEY `ezcontentobject_name_name` (`name`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

INSERT INTO `ezcontentobject_name` (`contentobject_id`, `content_version`, `content_translation`, `language_id`, `name`, `real_translation`) VALUES
(1,	9,	'eng-GB',	2,	'Ibexa Platform',	'eng-GB'),
(4,	1,	'eng-GB',	3,	'Users',	'eng-GB'),
(10,	2,	'eng-GB',	3,	'Anonymous User',	'eng-GB'),
(11,	1,	'eng-GB',	3,	'Guest accounts',	'eng-GB'),
(12,	1,	'eng-GB',	3,	'Administrator users',	'eng-GB'),
(13,	1,	'eng-GB',	3,	'Editors',	'eng-GB'),
(14,	3,	'eng-GB',	3,	'Administrator User',	'eng-GB'),
(41,	1,	'eng-GB',	3,	'Media',	'eng-GB'),
(42,	1,	'eng-GB',	3,	'Anonymous Users',	'eng-GB'),
(49,	1,	'eng-GB',	3,	'Images',	'eng-GB'),
(50,	1,	'eng-GB',	3,	'Files',	'eng-GB'),
(51,	1,	'eng-GB',	3,	'Multimedia',	'eng-GB'),
(52,	1,	'eng-GB',	2,	'Ibexa Digital Experience Platform',	'eng-GB'),
(53,	1,	'eng-GB',	2,	'Form Uploads',	'eng-GB'),
(54,	1,	'eng-GB',	3,	'Forms',	'eng-GB'),
(55,	1,	'eng-GB',	3,	'Site skeletons',	'eng-GB');

DROP TABLE IF EXISTS `ezcontentobject_trash`;
CREATE TABLE `ezcontentobject_trash` (
  `node_id` int(11) NOT NULL DEFAULT 0,
  `contentobject_id` int(11) DEFAULT NULL,
  `contentobject_version` int(11) DEFAULT NULL,
  `depth` int(11) NOT NULL DEFAULT 0,
  `is_hidden` int(11) NOT NULL DEFAULT 0,
  `is_invisible` int(11) NOT NULL DEFAULT 0,
  `main_node_id` int(11) DEFAULT NULL,
  `modified_subnode` int(11) DEFAULT 0,
  `parent_node_id` int(11) NOT NULL DEFAULT 0,
  `path_identification_string` longtext COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `path_string` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `priority` int(11) NOT NULL DEFAULT 0,
  `remote_id` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `sort_field` int(11) DEFAULT 1,
  `sort_order` int(11) DEFAULT 1,
  `trashed` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`node_id`),
  KEY `ezcobj_trash_depth` (`depth`),
  KEY `ezcobj_trash_p_node_id` (`parent_node_id`),
  KEY `ezcobj_trash_path_ident` (`path_identification_string`(50)),
  KEY `ezcobj_trash_co_id` (`contentobject_id`),
  KEY `ezcobj_trash_modified_subnode` (`modified_subnode`),
  KEY `ezcobj_trash_path` (`path_string`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


DROP TABLE IF EXISTS `ezcontentobject_tree`;
CREATE TABLE `ezcontentobject_tree` (
  `node_id` int(11) NOT NULL AUTO_INCREMENT,
  `contentobject_id` int(11) DEFAULT NULL,
  `contentobject_is_published` int(11) DEFAULT NULL,
  `contentobject_version` int(11) DEFAULT NULL,
  `depth` int(11) NOT NULL DEFAULT 0,
  `is_hidden` int(11) NOT NULL DEFAULT 0,
  `is_invisible` int(11) NOT NULL DEFAULT 0,
  `main_node_id` int(11) DEFAULT NULL,
  `modified_subnode` int(11) DEFAULT 0,
  `parent_node_id` int(11) NOT NULL DEFAULT 0,
  `path_identification_string` longtext COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `path_string` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `priority` int(11) NOT NULL DEFAULT 0,
  `remote_id` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `sort_field` int(11) DEFAULT 1,
  `sort_order` int(11) DEFAULT 1,
  PRIMARY KEY (`node_id`),
  KEY `ezcontentobject_tree_p_node_id` (`parent_node_id`),
  KEY `ezcontentobject_tree_path_ident` (`path_identification_string`(50)),
  KEY `ezcontentobject_tree_contentobject_id_path_string` (`path_string`(191),`contentobject_id`),
  KEY `ezcontentobject_tree_co_id` (`contentobject_id`),
  KEY `ezcontentobject_tree_depth` (`depth`),
  KEY `ezcontentobject_tree_path` (`path_string`(191)),
  KEY `modified_subnode` (`modified_subnode`),
  KEY `ezcontentobject_tree_remote_id` (`remote_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

INSERT INTO `ezcontentobject_tree` (`node_id`, `contentobject_id`, `contentobject_is_published`, `contentobject_version`, `depth`, `is_hidden`, `is_invisible`, `main_node_id`, `modified_subnode`, `parent_node_id`, `path_identification_string`, `path_string`, `priority`, `remote_id`, `sort_field`, `sort_order`) VALUES
(1,	0,	1,	1,	0,	0,	0,	1,	1448999778,	1,	'',	'/1/',	0,	'629709ba256fe317c3ddcee35453a96a',	1,	1),
(2,	52,	1,	1,	1,	0,	0,	2,	1301073466,	1,	'node_2',	'/1/2/',	0,	'f3e90596361e31d496d4026eb624c983',	8,	1),
(5,	4,	1,	1,	1,	0,	0,	5,	1301062024,	1,	'users',	'/1/5/',	0,	'3f6d92f8044aed134f32153517850f5a',	1,	1),
(12,	11,	1,	1,	2,	0,	0,	12,	1081860719,	5,	'users/guest_accounts',	'/1/5/12/',	0,	'602dcf84765e56b7f999eaafd3821dd3',	1,	1),
(13,	12,	1,	1,	2,	0,	0,	13,	1301062024,	5,	'users/administrator_users',	'/1/5/13/',	0,	'769380b7aa94541679167eab817ca893',	1,	1),
(14,	13,	1,	1,	2,	0,	0,	14,	1081860719,	5,	'users/editors',	'/1/5/14/',	0,	'f7dda2854fc68f7c8455d9cb14bd04a9',	1,	1),
(15,	14,	1,	3,	3,	0,	0,	15,	1301062024,	13,	'users/administrator_users/administrator_user',	'/1/5/13/15/',	0,	'e5161a99f733200b9ed4e80f9c16187b',	1,	1),
(42,	1,	1,	9,	2,	0,	0,	42,	1486473151,	2,	'node_2/ez_platform',	'/1/2/42/',	0,	'581da01017b80b1afb1e5e2a3081c724',	1,	1),
(43,	41,	1,	1,	1,	0,	0,	43,	1081860720,	1,	'media',	'/1/43/',	0,	'75c715a51699d2d309a924eca6a95145',	9,	1),
(44,	42,	1,	1,	2,	0,	0,	44,	1081860719,	5,	'users/anonymous_users',	'/1/5/44/',	0,	'4fdf0072da953bb276c0c7e0141c5c9b',	9,	1),
(45,	10,	1,	2,	3,	0,	0,	45,	1081860719,	44,	'users/anonymous_users/anonymous_user',	'/1/5/44/45/',	0,	'2cf8343bee7b482bab82b269d8fecd76',	9,	1),
(51,	49,	1,	1,	2,	0,	0,	51,	1081860720,	43,	'media/images',	'/1/43/51/',	0,	'1b26c0454b09bb49dfb1b9190ffd67cb',	9,	1),
(52,	50,	1,	1,	2,	0,	0,	52,	1081860720,	43,	'media/files',	'/1/43/52/',	0,	'0b113a208f7890f9ad3c24444ff5988c',	9,	1),
(53,	51,	1,	1,	2,	0,	0,	53,	1081860720,	43,	'media/multimedia',	'/1/43/53/',	0,	'4f18b82c75f10aad476cae5adf98c11f',	9,	1),
(54,	53,	1,	1,	3,	0,	0,	54,	1486473151,	52,	'media/files/form_uploads',	'/1/43/52/54/',	0,	'0543630fa051a1e2be54dbd32da2420f',	1,	1),
(55,	54,	1,	1,	1,	0,	0,	55,	1537166893,	1,	'forms',	'/1/55/',	0,	'1dad43be47e3a5c12cd06010aab65112',	9,	1),
(56,	55,	1,	1,	2,	0,	0,	56,	1586855342,	1,	'site_skeletons',	'/1/56/',	0,	'9658f6deaeef9fc27300df5d5f566b37',	9,	1);

DROP TABLE IF EXISTS `ezcontentobject_version`;
CREATE TABLE `ezcontentobject_version` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contentobject_id` int(11) DEFAULT NULL,
  `created` int(11) NOT NULL DEFAULT 0,
  `creator_id` int(11) NOT NULL DEFAULT 0,
  `initial_language_id` bigint(20) NOT NULL DEFAULT 0,
  `language_mask` bigint(20) NOT NULL DEFAULT 0,
  `modified` int(11) NOT NULL DEFAULT 0,
  `status` int(11) NOT NULL DEFAULT 0,
  `user_id` int(11) NOT NULL DEFAULT 0,
  `version` int(11) NOT NULL DEFAULT 0,
  `workflow_event_pos` int(11) DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `ezcobj_version_status` (`status`),
  KEY `idx_object_version_objver` (`contentobject_id`,`version`),
  KEY `ezcontobj_version_obj_status` (`contentobject_id`,`status`),
  KEY `ezcobj_version_creator_id` (`creator_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

INSERT INTO `ezcontentobject_version` (`id`, `contentobject_id`, `created`, `creator_id`, `initial_language_id`, `language_mask`, `modified`, `status`, `user_id`, `version`, `workflow_event_pos`) VALUES
(4,	4,	0,	14,	2,	3,	0,	1,	0,	1,	1),
(439,	11,	1033920737,	14,	2,	3,	1033920746,	1,	0,	1,	0),
(440,	12,	1033920760,	14,	2,	3,	1033920775,	1,	0,	1,	0),
(441,	13,	1033920786,	14,	2,	3,	1033920794,	1,	0,	1,	0),
(472,	41,	1060695450,	14,	2,	3,	1060695457,	1,	0,	1,	0),
(473,	42,	1072180278,	14,	2,	3,	1072180330,	1,	0,	1,	0),
(474,	10,	1072180337,	14,	2,	3,	1072180405,	1,	0,	2,	0),
(488,	49,	1080220181,	14,	2,	3,	1080220197,	1,	0,	1,	0),
(489,	50,	1080220211,	14,	2,	3,	1080220220,	1,	0,	1,	0),
(490,	51,	1080220225,	14,	2,	3,	1080220233,	1,	0,	1,	0),
(499,	14,	1301061783,	14,	2,	3,	1301062024,	1,	0,	3,	0),
(506,	1,	1448889045,	14,	2,	3,	1448889046,	1,	0,	9,	0),
(512,	52,	1442481742,	14,	2,	3,	1442481743,	1,	0,	1,	0),
(513,	53,	1486473143,	14,	2,	3,	1486473151,	1,	0,	1,	0),
(514,	54,	1537166893,	14,	2,	2,	1537166893,	1,	0,	1,	0),
(515,	55,	1586855342,	14,	2,	3,	1586855342,	1,	0,	1,	0);

DROP TABLE IF EXISTS `ezcontent_language`;
CREATE TABLE `ezcontent_language` (
  `id` bigint(20) NOT NULL DEFAULT 0,
  `disabled` int(11) NOT NULL DEFAULT 0,
  `locale` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `ezcontent_language_name` (`name`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

INSERT INTO `ezcontent_language` (`id`, `disabled`, `locale`, `name`) VALUES
(2,	0,	'eng-GB',	'English (United Kingdom)');

DROP TABLE IF EXISTS `ezdatebasedpublisher_scheduled_entries`;
CREATE TABLE `ezdatebasedpublisher_scheduled_entries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content_id` int(11) NOT NULL,
  `version_id` int(11) DEFAULT NULL,
  `version_number` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `action_timestamp` int(11) NOT NULL,
  `action` varchar(32) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `url_root` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `content_id_version_number_action` (`content_id`,`version_number`,`action`),
  KEY `content_id` (`content_id`),
  KEY `version_id` (`version_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


DROP TABLE IF EXISTS `ezdfsfile`;
CREATE TABLE `ezdfsfile` (
  `name_hash` varchar(34) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `name` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `name_trunk` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `datatype` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'application/octet-stream',
  `scope` varchar(25) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `size` bigint(20) unsigned NOT NULL DEFAULT 0,
  `mtime` int(11) NOT NULL DEFAULT 0,
  `expired` tinyint(1) NOT NULL DEFAULT 0,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`name_hash`),
  KEY `ezdfsfile_name_trunk` (`name_trunk`(191)),
  KEY `ezdfsfile_expired_name` (`expired`,`name`(191)),
  KEY `ezdfsfile_name` (`name`(191)),
  KEY `ezdfsfile_mtime` (`mtime`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


DROP TABLE IF EXISTS `ezeditorialworkflow_markings`;
CREATE TABLE `ezeditorialworkflow_markings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `workflow_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `message` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `reviewer_id` int(11) DEFAULT NULL,
  `result` text COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_workflow_id_markings` (`workflow_id`),
  CONSTRAINT `fk_ezeditorialworkflow_markings_workflow_id` FOREIGN KEY (`workflow_id`) REFERENCES `ezeditorialworkflow_workflows` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


DROP TABLE IF EXISTS `ezeditorialworkflow_transitions`;
CREATE TABLE `ezeditorialworkflow_transitions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `workflow_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `timestamp` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `comment` text COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_workflow_id_transitions` (`workflow_id`),
  CONSTRAINT `fk_ezeditorialworkflow_transitions_workflow_id` FOREIGN KEY (`workflow_id`) REFERENCES `ezeditorialworkflow_workflows` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


DROP TABLE IF EXISTS `ezeditorialworkflow_workflows`;
CREATE TABLE `ezeditorialworkflow_workflows` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content_id` int(11) NOT NULL,
  `version_no` int(11) NOT NULL,
  `workflow_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `initial_owner_id` int(11) DEFAULT NULL,
  `start_date` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_workflow_id` (`id`),
  KEY `idx_workflow_co_id_ver` (`content_id`,`version_no`),
  KEY `idx_workflow_name` (`workflow_name`),
  KEY `initial_owner_id` (`initial_owner_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


DROP TABLE IF EXISTS `ezform_fields`;
CREATE TABLE `ezform_fields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `form_id` int(11) DEFAULT NULL,
  `name` varchar(128) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `identifier` varchar(128) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


DROP TABLE IF EXISTS `ezform_field_attributes`;
CREATE TABLE `ezform_field_attributes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `field_id` int(11) DEFAULT NULL,
  `identifier` varchar(128) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `value` text COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


DROP TABLE IF EXISTS `ezform_field_validators`;
CREATE TABLE `ezform_field_validators` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `field_id` int(11) DEFAULT NULL,
  `identifier` varchar(128) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `value` text COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


DROP TABLE IF EXISTS `ezform_forms`;
CREATE TABLE `ezform_forms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content_id` int(11) DEFAULT NULL,
  `version_no` int(11) DEFAULT NULL,
  `content_field_id` int(11) DEFAULT NULL,
  `language_code` varchar(16) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


DROP TABLE IF EXISTS `ezform_form_submissions`;
CREATE TABLE `ezform_form_submissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content_id` int(11) NOT NULL,
  `language_code` varchar(6) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `created` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


DROP TABLE IF EXISTS `ezform_form_submission_data`;
CREATE TABLE `ezform_form_submission_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `form_submission_id` int(11) NOT NULL,
  `name` varchar(128) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `identifier` varchar(128) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


DROP TABLE IF EXISTS `ezgmaplocation`;
CREATE TABLE `ezgmaplocation` (
  `contentobject_attribute_id` int(11) NOT NULL DEFAULT 0,
  `contentobject_version` int(11) NOT NULL DEFAULT 0,
  `latitude` double NOT NULL DEFAULT 0,
  `longitude` double NOT NULL DEFAULT 0,
  `address` varchar(150) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  PRIMARY KEY (`contentobject_attribute_id`,`contentobject_version`),
  KEY `latitude_longitude_key` (`latitude`,`longitude`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


DROP TABLE IF EXISTS `ezimagefile`;
CREATE TABLE `ezimagefile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contentobject_attribute_id` int(11) NOT NULL DEFAULT 0,
  `filepath` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ezimagefile_file` (`filepath`(191)),
  KEY `ezimagefile_coid` (`contentobject_attribute_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


DROP TABLE IF EXISTS `ezkeyword`;
CREATE TABLE `ezkeyword` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `class_id` int(11) NOT NULL DEFAULT 0,
  `keyword` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ezkeyword_keyword` (`keyword`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


DROP TABLE IF EXISTS `ezkeyword_attribute_link`;
CREATE TABLE `ezkeyword_attribute_link` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `keyword_id` int(11) NOT NULL DEFAULT 0,
  `objectattribute_id` int(11) NOT NULL DEFAULT 0,
  `version` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `ezkeyword_attr_link_oaid` (`objectattribute_id`),
  KEY `ezkeyword_attr_link_kid_oaid` (`keyword_id`,`objectattribute_id`),
  KEY `ezkeyword_attr_link_oaid_ver` (`objectattribute_id`,`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


DROP TABLE IF EXISTS `ezmedia`;
CREATE TABLE `ezmedia` (
  `contentobject_attribute_id` int(11) NOT NULL DEFAULT 0,
  `version` int(11) NOT NULL DEFAULT 0,
  `controls` varchar(50) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `filename` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `has_controller` int(11) DEFAULT 0,
  `height` int(11) DEFAULT NULL,
  `is_autoplay` int(11) DEFAULT 0,
  `is_loop` int(11) DEFAULT 0,
  `mime_type` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `original_filename` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `pluginspage` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `quality` varchar(50) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `width` int(11) DEFAULT NULL,
  PRIMARY KEY (`contentobject_attribute_id`,`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


DROP TABLE IF EXISTS `eznode_assignment`;
CREATE TABLE `eznode_assignment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contentobject_id` int(11) DEFAULT NULL,
  `contentobject_version` int(11) DEFAULT NULL,
  `from_node_id` int(11) DEFAULT 0,
  `is_main` int(11) NOT NULL DEFAULT 0,
  `op_code` int(11) NOT NULL DEFAULT 0,
  `parent_node` int(11) DEFAULT NULL,
  `parent_remote_id` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `remote_id` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '0',
  `sort_field` int(11) DEFAULT 1,
  `sort_order` int(11) DEFAULT 1,
  `priority` int(11) NOT NULL DEFAULT 0,
  `is_hidden` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `eznode_assignment_is_main` (`is_main`),
  KEY `eznode_assignment_coid_cov` (`contentobject_id`,`contentobject_version`),
  KEY `eznode_assignment_parent_node` (`parent_node`),
  KEY `eznode_assignment_co_version` (`contentobject_version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

INSERT INTO `eznode_assignment` (`id`, `contentobject_id`, `contentobject_version`, `from_node_id`, `is_main`, `op_code`, `parent_node`, `parent_remote_id`, `remote_id`, `sort_field`, `sort_order`, `priority`, `is_hidden`) VALUES
(4,	8,	2,	0,	1,	2,	5,	'',	'0',	1,	1,	0,	0),
(5,	42,	1,	0,	1,	2,	5,	'',	'0',	9,	1,	0,	0),
(6,	10,	2,	-1,	1,	2,	44,	'',	'0',	9,	1,	0,	0),
(7,	4,	1,	0,	1,	2,	1,	'',	'0',	1,	1,	0,	0),
(8,	12,	1,	0,	1,	2,	5,	'',	'0',	1,	1,	0,	0),
(9,	13,	1,	0,	1,	2,	5,	'',	'0',	1,	1,	0,	0),
(11,	41,	1,	0,	1,	2,	1,	'',	'0',	1,	1,	0,	0),
(12,	11,	1,	0,	1,	2,	5,	'',	'0',	1,	1,	0,	0),
(27,	49,	1,	0,	1,	2,	43,	'',	'0',	9,	1,	0,	0),
(28,	50,	1,	0,	1,	2,	43,	'',	'0',	9,	1,	0,	0),
(29,	51,	1,	0,	1,	2,	43,	'',	'0',	9,	1,	0,	0),
(38,	14,	3,	-1,	1,	2,	13,	'',	'0',	1,	1,	0,	0),
(40,	53,	1,	0,	1,	2,	52,	'0543630fa051a1e2be54dbd32da2420f',	'0',	1,	1,	0,	0),
(41,	54,	1,	0,	1,	2,	2,	'1dad43be47e3a5c12cd06010aab65112',	'0',	9,	1,	0,	0),
(42,	55,	1,	0,	1,	2,	2,	'9658f6deaeef9fc27300df5d5f566b37',	'0',	9,	1,	0,	0);

DROP TABLE IF EXISTS `eznotification`;
CREATE TABLE `eznotification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) NOT NULL DEFAULT 0,
  `is_pending` tinyint(1) NOT NULL DEFAULT 1,
  `type` varchar(128) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `created` int(11) NOT NULL DEFAULT 0,
  `data` longtext COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `eznotification_owner_is_pending` (`owner_id`,`is_pending`),
  KEY `eznotification_owner` (`owner_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


DROP TABLE IF EXISTS `ezpackage`;
CREATE TABLE `ezpackage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `install_date` int(11) NOT NULL DEFAULT 0,
  `name` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `version` varchar(30) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


DROP TABLE IF EXISTS `ezpage_attributes`;
CREATE TABLE `ezpage_attributes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `value` longtext COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

INSERT INTO `ezpage_attributes` (`id`, `name`, `value`) VALUES
(1,	'content',	'<p>This is the clean installation coming with Ibexa Digital Experience Platform.<br>It\'s a bare-bones setup of the Platform, an excellent foundation to build upon if you want to start your project from scratch.</p>');

DROP TABLE IF EXISTS `ezpage_blocks`;
CREATE TABLE `ezpage_blocks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `view` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

INSERT INTO `ezpage_blocks` (`id`, `type`, `view`, `name`) VALUES
(1,	'tag',	'default',	'Tag');

DROP TABLE IF EXISTS `ezpage_blocks_design`;
CREATE TABLE `ezpage_blocks_design` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `block_id` int(11) NOT NULL,
  `style` text COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `compiled` text COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `class` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ezpage_blocks_design_block_id` (`block_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


DROP TABLE IF EXISTS `ezpage_blocks_visibility`;
CREATE TABLE `ezpage_blocks_visibility` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `block_id` int(11) NOT NULL,
  `since` int(11) DEFAULT NULL,
  `till` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ezpage_blocks_visibility_block_id` (`block_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


DROP TABLE IF EXISTS `ezpage_map_attributes_blocks`;
CREATE TABLE `ezpage_map_attributes_blocks` (
  `attribute_id` int(11) NOT NULL,
  `block_id` int(11) NOT NULL,
  PRIMARY KEY (`attribute_id`,`block_id`),
  KEY `ezpage_map_attributes_blocks_attribute_id` (`attribute_id`),
  KEY `ezpage_map_attributes_blocks_block_id` (`block_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

INSERT INTO `ezpage_map_attributes_blocks` (`attribute_id`, `block_id`) VALUES
(1,	1);

DROP TABLE IF EXISTS `ezpage_map_blocks_zones`;
CREATE TABLE `ezpage_map_blocks_zones` (
  `block_id` int(11) NOT NULL,
  `zone_id` int(11) NOT NULL,
  PRIMARY KEY (`block_id`,`zone_id`),
  KEY `ezpage_map_blocks_zones_block_id` (`block_id`),
  KEY `ezpage_map_blocks_zones_zone_id` (`zone_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

INSERT INTO `ezpage_map_blocks_zones` (`block_id`, `zone_id`) VALUES
(1,	1);

DROP TABLE IF EXISTS `ezpage_map_zones_pages`;
CREATE TABLE `ezpage_map_zones_pages` (
  `zone_id` int(11) NOT NULL,
  `page_id` int(11) NOT NULL,
  PRIMARY KEY (`zone_id`,`page_id`),
  KEY `ezpage_map_zones_pages_zone_id` (`zone_id`),
  KEY `ezpage_map_zones_pages_page_id` (`page_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

INSERT INTO `ezpage_map_zones_pages` (`zone_id`, `page_id`) VALUES
(1,	1);

DROP TABLE IF EXISTS `ezpage_pages`;
CREATE TABLE `ezpage_pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `version_no` int(11) NOT NULL,
  `content_id` int(11) NOT NULL,
  `language_code` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `layout` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `ezpage_pages_content_id_version_no` (`content_id`,`version_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

INSERT INTO `ezpage_pages` (`id`, `version_no`, `content_id`, `language_code`, `layout`) VALUES
(1,	1,	52,	'eng-GB',	'default');

DROP TABLE IF EXISTS `ezpage_zones`;
CREATE TABLE `ezpage_zones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

INSERT INTO `ezpage_zones` (`id`, `name`) VALUES
(1,	'default');

DROP TABLE IF EXISTS `ezpolicy`;
CREATE TABLE `ezpolicy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `function_name` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `module_name` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `original_id` int(11) NOT NULL DEFAULT 0,
  `role_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ezpolicy_role_id` (`role_id`),
  KEY `ezpolicy_original_id` (`original_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

INSERT INTO `ezpolicy` (`id`, `function_name`, `module_name`, `original_id`, `role_id`) VALUES
(317,	'*',	'content',	0,	3),
(319,	'login',	'user',	0,	3),
(328,	'read',	'content',	0,	1),
(331,	'login',	'user',	0,	1),
(332,	'*',	'*',	0,	2),
(333,	'read',	'content',	0,	4),
(334,	'view_embed',	'content',	0,	1),
(340,	'*',	'url',	0,	3);

DROP TABLE IF EXISTS `ezpolicy_limitation`;
CREATE TABLE `ezpolicy_limitation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identifier` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `policy_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `policy_id` (`policy_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

INSERT INTO `ezpolicy_limitation` (`id`, `identifier`, `policy_id`) VALUES
(251,	'Section',	328),
(252,	'Section',	329),
(253,	'SiteAccess',	331),
(254,	'Class',	333),
(255,	'Owner',	333),
(256,	'Class',	334);

DROP TABLE IF EXISTS `ezpolicy_limitation_value`;
CREATE TABLE `ezpolicy_limitation_value` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `limitation_id` int(11) DEFAULT NULL,
  `value` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ezpolicy_limit_value_limit_id` (`limitation_id`),
  KEY `ezpolicy_limitation_value_val` (`value`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

INSERT INTO `ezpolicy_limitation_value` (`id`, `limitation_id`, `value`) VALUES
(477,	251,	'1'),
(478,	252,	'1'),
(479,	253,	'1766001124'),
(480,	254,	'4'),
(481,	255,	'1'),
(482,	256,	'5'),
(483,	256,	'12'),
(484,	251,	'3'),
(485,	251,	'6');

DROP TABLE IF EXISTS `ezpreferences`;
CREATE TABLE `ezpreferences` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `user_id` int(11) NOT NULL DEFAULT 0,
  `value` longtext COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ezpreferences_user_id_idx` (`user_id`,`name`),
  KEY `ezpreferences_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


DROP TABLE IF EXISTS `ezrole`;
CREATE TABLE `ezrole` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `is_new` int(11) NOT NULL DEFAULT 0,
  `name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `value` char(1) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `version` int(11) DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

INSERT INTO `ezrole` (`id`, `is_new`, `name`, `value`, `version`) VALUES
(1,	0,	'Anonymous',	'',	0),
(2,	0,	'Administrator',	'0',	0),
(3,	0,	'Editor',	'',	0),
(4,	0,	'Member',	'',	0);

DROP TABLE IF EXISTS `ezsearch_object_word_link`;
CREATE TABLE `ezsearch_object_word_link` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contentclass_attribute_id` int(11) NOT NULL DEFAULT 0,
  `contentclass_id` int(11) NOT NULL DEFAULT 0,
  `contentobject_id` int(11) NOT NULL DEFAULT 0,
  `frequency` double NOT NULL DEFAULT 0,
  `identifier` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `integer_value` int(11) NOT NULL DEFAULT 0,
  `next_word_id` int(11) NOT NULL DEFAULT 0,
  `placement` int(11) NOT NULL DEFAULT 0,
  `prev_word_id` int(11) NOT NULL DEFAULT 0,
  `published` int(11) NOT NULL DEFAULT 0,
  `section_id` int(11) NOT NULL DEFAULT 0,
  `word_id` int(11) NOT NULL DEFAULT 0,
  `language_mask` bigint(20) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `ezsearch_object_word_link_object` (`contentobject_id`),
  KEY `ezsearch_object_word_link_identifier` (`identifier`(191)),
  KEY `ezsearch_object_word_link_integer_value` (`integer_value`),
  KEY `ezsearch_object_word_link_word` (`word_id`),
  KEY `ezsearch_object_word_link_frequency` (`frequency`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

INSERT INTO `ezsearch_object_word_link` (`id`, `contentclass_attribute_id`, `contentclass_id`, `contentobject_id`, `frequency`, `identifier`, `integer_value`, `next_word_id`, `placement`, `prev_word_id`, `published`, `section_id`, `word_id`, `language_mask`) VALUES
(1,	4,	1,	1,	0,	'name',	0,	2,	0,	0,	1448889046,	1,	1,	3),
(2,	4,	1,	1,	0,	'name',	0,	3,	1,	1,	1448889046,	1,	2,	3),
(3,	119,	1,	1,	0,	'short_description',	0,	4,	2,	2,	1448889046,	1,	3,	3),
(4,	119,	1,	1,	0,	'short_description',	0,	5,	3,	3,	1448889046,	1,	4,	3),
(5,	119,	1,	1,	0,	'short_description',	0,	6,	4,	4,	1448889046,	1,	5,	3),
(6,	119,	1,	1,	0,	'short_description',	0,	7,	5,	5,	1448889046,	1,	6,	3),
(7,	119,	1,	1,	0,	'short_description',	0,	8,	6,	6,	1448889046,	1,	7,	3),
(8,	119,	1,	1,	0,	'short_description',	0,	9,	7,	7,	1448889046,	1,	8,	3),
(9,	119,	1,	1,	0,	'short_description',	0,	10,	8,	8,	1448889046,	1,	9,	3),
(10,	119,	1,	1,	0,	'short_description',	0,	1,	9,	9,	1448889046,	1,	10,	3),
(11,	155,	1,	1,	0,	'short_name',	0,	2,	10,	10,	1448889046,	1,	1,	3),
(12,	155,	1,	1,	0,	'short_name',	0,	11,	11,	1,	1448889046,	1,	2,	3),
(13,	156,	1,	1,	0,	'description',	0,	12,	12,	2,	1448889046,	1,	11,	3),
(14,	156,	1,	1,	0,	'description',	0,	13,	13,	11,	1448889046,	1,	12,	3),
(15,	156,	1,	1,	0,	'description',	0,	14,	14,	12,	1448889046,	1,	13,	3),
(16,	156,	1,	1,	0,	'description',	0,	15,	15,	13,	1448889046,	1,	14,	3),
(17,	156,	1,	1,	0,	'description',	0,	16,	16,	14,	1448889046,	1,	15,	3),
(18,	156,	1,	1,	0,	'description',	0,	17,	17,	15,	1448889046,	1,	16,	3),
(19,	156,	1,	1,	0,	'description',	0,	1,	18,	16,	1448889046,	1,	17,	3),
(20,	156,	1,	1,	0,	'description',	0,	2,	19,	17,	1448889046,	1,	1,	3),
(21,	156,	1,	1,	0,	'description',	0,	18,	20,	1,	1448889046,	1,	2,	3),
(22,	156,	1,	1,	0,	'description',	0,	19,	21,	2,	1448889046,	1,	18,	3),
(23,	156,	1,	1,	0,	'description',	0,	20,	22,	18,	1448889046,	1,	19,	3),
(24,	156,	1,	1,	0,	'description',	0,	21,	23,	19,	1448889046,	1,	20,	3),
(25,	156,	1,	1,	0,	'description',	0,	22,	24,	20,	1448889046,	1,	21,	3),
(26,	156,	1,	1,	0,	'description',	0,	23,	25,	21,	1448889046,	1,	22,	3),
(27,	156,	1,	1,	0,	'description',	0,	24,	26,	22,	1448889046,	1,	23,	3),
(28,	156,	1,	1,	0,	'description',	0,	13,	27,	23,	1448889046,	1,	24,	3),
(29,	156,	1,	1,	0,	'description',	0,	2,	28,	24,	1448889046,	1,	13,	3),
(30,	156,	1,	1,	0,	'description',	0,	25,	29,	13,	1448889046,	1,	2,	3),
(31,	156,	1,	1,	0,	'description',	0,	26,	30,	2,	1448889046,	1,	25,	3),
(32,	156,	1,	1,	0,	'description',	0,	27,	31,	25,	1448889046,	1,	26,	3),
(33,	156,	1,	1,	0,	'description',	0,	7,	32,	26,	1448889046,	1,	27,	3),
(34,	156,	1,	1,	0,	'description',	0,	28,	33,	27,	1448889046,	1,	7,	3),
(35,	156,	1,	1,	0,	'description',	0,	29,	34,	7,	1448889046,	1,	28,	3),
(36,	156,	1,	1,	0,	'description',	0,	30,	35,	28,	1448889046,	1,	29,	3),
(37,	156,	1,	1,	0,	'description',	0,	3,	36,	29,	1448889046,	1,	30,	3),
(38,	156,	1,	1,	0,	'description',	0,	31,	37,	30,	1448889046,	1,	3,	3),
(39,	156,	1,	1,	0,	'description',	0,	7,	38,	3,	1448889046,	1,	31,	3),
(40,	156,	1,	1,	0,	'description',	0,	8,	39,	31,	1448889046,	1,	7,	3),
(41,	156,	1,	1,	0,	'description',	0,	9,	40,	7,	1448889046,	1,	8,	3),
(42,	156,	1,	1,	0,	'description',	0,	10,	41,	8,	1448889046,	1,	9,	3),
(43,	156,	1,	1,	0,	'description',	0,	32,	42,	9,	1448889046,	1,	10,	3),
(44,	156,	1,	1,	0,	'description',	0,	33,	43,	10,	1448889046,	1,	32,	3),
(45,	156,	1,	1,	0,	'description',	0,	0,	44,	32,	1448889046,	1,	33,	3),
(46,	7,	3,	4,	0,	'description',	0,	35,	0,	0,	1033917596,	2,	34,	3),
(47,	7,	3,	4,	0,	'description',	0,	36,	1,	34,	1033917596,	2,	35,	3),
(48,	6,	3,	4,	0,	'name',	0,	0,	2,	35,	1033917596,	2,	36,	3),
(49,	8,	4,	10,	0,	'first_name',	0,	38,	0,	0,	1033920665,	2,	37,	3),
(50,	9,	4,	10,	0,	'last_name',	0,	0,	1,	37,	1033920665,	2,	38,	3),
(51,	6,	3,	11,	0,	'name',	0,	40,	0,	0,	1033920746,	2,	39,	3),
(52,	6,	3,	11,	0,	'name',	0,	0,	1,	39,	1033920746,	2,	40,	3),
(53,	6,	3,	12,	0,	'name',	0,	36,	0,	0,	1033920775,	2,	41,	3),
(54,	6,	3,	12,	0,	'name',	0,	0,	1,	41,	1033920775,	2,	36,	3),
(55,	6,	3,	13,	0,	'name',	0,	0,	0,	0,	1033920794,	2,	42,	3),
(56,	8,	4,	14,	0,	'first_name',	0,	38,	0,	0,	1033920830,	2,	41,	3),
(57,	9,	4,	14,	0,	'last_name',	0,	0,	1,	41,	1033920830,	2,	38,	3),
(58,	4,	1,	41,	0,	'name',	0,	0,	0,	0,	1060695457,	3,	43,	3),
(59,	6,	3,	42,	0,	'name',	0,	36,	0,	0,	1072180330,	2,	37,	3),
(60,	6,	3,	42,	0,	'name',	0,	38,	1,	37,	1072180330,	2,	36,	3),
(61,	7,	3,	42,	0,	'description',	0,	35,	2,	36,	1072180330,	2,	38,	3),
(62,	7,	3,	42,	0,	'description',	0,	44,	3,	38,	1072180330,	2,	35,	3),
(63,	7,	3,	42,	0,	'description',	0,	13,	4,	35,	1072180330,	2,	44,	3),
(64,	7,	3,	42,	0,	'description',	0,	37,	5,	44,	1072180330,	2,	13,	3),
(65,	7,	3,	42,	0,	'description',	0,	38,	6,	13,	1072180330,	2,	37,	3),
(66,	7,	3,	42,	0,	'description',	0,	0,	7,	37,	1072180330,	2,	38,	3),
(67,	4,	1,	49,	0,	'name',	0,	0,	0,	0,	1080220197,	3,	45,	3),
(68,	4,	1,	50,	0,	'name',	0,	0,	0,	0,	1080220220,	3,	46,	3),
(69,	4,	1,	51,	0,	'name',	0,	0,	0,	0,	1080220233,	3,	47,	3),
(70,	185,	42,	52,	0,	'name',	0,	48,	0,	0,	1442481743,	1,	1,	3),
(71,	185,	42,	52,	0,	'name',	0,	49,	1,	1,	1442481743,	1,	48,	3),
(72,	185,	42,	52,	0,	'name',	0,	2,	2,	48,	1442481743,	1,	49,	3),
(73,	185,	42,	52,	0,	'name',	0,	3,	3,	49,	1442481743,	1,	2,	3),
(74,	186,	42,	52,	0,	'description',	0,	4,	4,	2,	1442481743,	1,	3,	3),
(75,	186,	42,	52,	0,	'description',	0,	5,	5,	3,	1442481743,	1,	4,	3),
(76,	186,	42,	52,	0,	'description',	0,	6,	6,	4,	1442481743,	1,	5,	3),
(77,	186,	42,	52,	0,	'description',	0,	7,	7,	5,	1442481743,	1,	6,	3),
(78,	186,	42,	52,	0,	'description',	0,	8,	8,	6,	1442481743,	1,	7,	3),
(79,	186,	42,	52,	0,	'description',	0,	9,	9,	7,	1442481743,	1,	8,	3),
(80,	186,	42,	52,	0,	'description',	0,	10,	10,	8,	1442481743,	1,	9,	3),
(81,	186,	42,	52,	0,	'description',	0,	0,	11,	9,	1442481743,	1,	10,	3),
(82,	4,	1,	53,	0,	'name',	0,	51,	0,	0,	1486473151,	3,	50,	3),
(83,	4,	1,	53,	0,	'name',	0,	50,	1,	50,	1486473151,	3,	51,	3),
(84,	155,	1,	53,	0,	'short_name',	0,	51,	2,	51,	1486473151,	3,	50,	3),
(85,	155,	1,	53,	0,	'short_name',	0,	52,	3,	50,	1486473151,	3,	51,	3),
(86,	119,	1,	53,	0,	'short_description',	0,	44,	4,	51,	1486473151,	3,	52,	3),
(87,	119,	1,	53,	0,	'short_description',	0,	53,	5,	52,	1486473151,	3,	44,	3),
(88,	119,	1,	53,	0,	'short_description',	0,	51,	6,	44,	1486473151,	3,	53,	3),
(89,	119,	1,	53,	0,	'short_description',	0,	0,	7,	53,	1486473151,	3,	51,	3),
(90,	4,	1,	54,	0,	'name',	0,	0,	0,	0,	1537166893,	6,	54,	2),
(91,	4,	1,	55,	0,	'name',	0,	56,	0,	0,	1586855342,	7,	55,	3),
(92,	4,	1,	55,	0,	'name',	0,	0,	1,	55,	1586855342,	7,	56,	3);

DROP TABLE IF EXISTS `ezsearch_word`;
CREATE TABLE `ezsearch_word` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `object_count` int(11) NOT NULL DEFAULT 0,
  `word` varchar(150) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ezsearch_word_word_i` (`word`),
  KEY `ezsearch_word_obj_count` (`object_count`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

INSERT INTO `ezsearch_word` (`id`, `object_count`, `word`) VALUES
(1,	2,	'ibexa'),
(2,	2,	'platform'),
(3,	2,	'you'),
(4,	2,	'are'),
(5,	2,	'now'),
(6,	2,	'ready'),
(7,	2,	'to'),
(8,	2,	'start'),
(9,	2,	'your'),
(10,	2,	'project'),
(11,	1,	'this'),
(12,	1,	'is'),
(13,	2,	'the'),
(14,	1,	'clean'),
(15,	1,	'installation'),
(16,	1,	'coming'),
(17,	1,	'with'),
(18,	1,	'it'),
(19,	1,	's'),
(20,	1,	'a'),
(21,	1,	'bare'),
(22,	1,	'bones'),
(23,	1,	'setup'),
(24,	1,	'of'),
(25,	1,	'an'),
(26,	1,	'excellent'),
(27,	1,	'foundation'),
(28,	1,	'build'),
(29,	1,	'upon'),
(30,	1,	'if'),
(31,	1,	'want'),
(32,	1,	'from'),
(33,	1,	'scratch'),
(34,	1,	'main'),
(35,	2,	'group'),
(36,	3,	'users'),
(37,	2,	'anonymous'),
(38,	3,	'user'),
(39,	1,	'guest'),
(40,	1,	'accounts'),
(41,	2,	'administrator'),
(42,	1,	'editors'),
(43,	1,	'media'),
(44,	2,	'for'),
(45,	1,	'images'),
(46,	1,	'files'),
(47,	1,	'multimedia'),
(48,	1,	'digital'),
(49,	1,	'experience'),
(50,	1,	'form'),
(51,	1,	'uploads'),
(52,	1,	'folder'),
(53,	1,	'file'),
(54,	1,	'forms'),
(55,	1,	'site'),
(56,	1,	'skeletons');

DROP TABLE IF EXISTS `ezsection`;
CREATE TABLE `ezsection` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identifier` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `locale` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `navigation_part_identifier` varchar(100) COLLATE utf8mb4_unicode_520_ci DEFAULT 'ezcontentnavigationpart',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

INSERT INTO `ezsection` (`id`, `identifier`, `locale`, `name`, `navigation_part_identifier`) VALUES
(1,	'standard',	'',	'Standard',	'ezcontentnavigationpart'),
(2,	'users',	'',	'Users',	'ezusernavigationpart'),
(3,	'media',	'',	'Media',	'ezmedianavigationpart'),
(6,	'form',	NULL,	'Form',	'ezcontentnavigationpart'),
(7,	'site_skeleton',	NULL,	'Site skeleton',	'ezcontentnavigationpart');

DROP TABLE IF EXISTS `ezsite`;
CREATE TABLE `ezsite` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `created` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


DROP TABLE IF EXISTS `ezsite_data`;
CREATE TABLE `ezsite_data` (
  `name` varchar(60) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `value` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

INSERT INTO `ezsite_data` (`name`, `value`) VALUES
('ezplatform-release',	'3.0.0');

DROP TABLE IF EXISTS `ezsite_public_access`;
CREATE TABLE `ezsite_public_access` (
  `public_access_identifier` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `site_id` int(11) NOT NULL,
  `site_access_group` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `status` int(11) NOT NULL,
  `config` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `site_matcher_host` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `site_matcher_path` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  PRIMARY KEY (`public_access_identifier`),
  KEY `ezsite_public_access_site_id` (`site_id`),
  CONSTRAINT `fk_ezsite_public_access_site_id` FOREIGN KEY (`site_id`) REFERENCES `ezsite` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


DROP TABLE IF EXISTS `ezurl`;
CREATE TABLE `ezurl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created` int(11) NOT NULL DEFAULT 0,
  `is_valid` int(11) NOT NULL DEFAULT 1,
  `last_checked` int(11) NOT NULL DEFAULT 0,
  `modified` int(11) NOT NULL DEFAULT 0,
  `original_url_md5` varchar(32) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `url` longtext COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ezurl_url` (`url`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


DROP TABLE IF EXISTS `ezurlalias`;
CREATE TABLE `ezurlalias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `destination_url` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `forward_to_id` int(11) NOT NULL DEFAULT 0,
  `is_imported` int(11) NOT NULL DEFAULT 0,
  `is_internal` int(11) NOT NULL DEFAULT 1,
  `is_wildcard` int(11) NOT NULL DEFAULT 0,
  `source_md5` varchar(32) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `source_url` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ezurlalias_source_md5` (`source_md5`),
  KEY `ezurlalias_wcard_fwd` (`is_wildcard`,`forward_to_id`),
  KEY `ezurlalias_forward_to_id` (`forward_to_id`),
  KEY `ezurlalias_imp_wcard_fwd` (`is_imported`,`is_wildcard`,`forward_to_id`),
  KEY `ezurlalias_source_url` (`source_url`(191)),
  KEY `ezurlalias_desturl` (`destination_url`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

INSERT INTO `ezurlalias` (`id`, `destination_url`, `forward_to_id`, `is_imported`, `is_internal`, `is_wildcard`, `source_md5`, `source_url`) VALUES
(12,	'content/view/full/2',	0,	1,	1,	0,	'd41d8cd98f00b204e9800998ecf8427e',	''),
(13,	'content/view/full/5',	0,	1,	1,	0,	'9bc65c2abec141778ffaa729489f3e87',	'users'),
(15,	'content/view/full/12',	0,	1,	1,	0,	'02d4e844e3a660857a3f81585995ffe1',	'users/guest_accounts'),
(16,	'content/view/full/13',	0,	1,	1,	0,	'1b1d79c16700fd6003ea7be233e754ba',	'users/administrator_users'),
(17,	'content/view/full/14',	0,	1,	1,	0,	'0bb9dd665c96bbc1cf36b79180786dea',	'users/editors'),
(18,	'content/view/full/15',	0,	1,	1,	0,	'f1305ac5f327a19b451d82719e0c3f5d',	'users/administrator_users/administrator_user'),
(20,	'content/view/full/43',	0,	1,	1,	0,	'62933a2951ef01f4eafd9bdf4d3cd2f0',	'media'),
(21,	'content/view/full/44',	0,	1,	1,	0,	'3ae1aac958e1c82013689d917d34967a',	'users/anonymous_users'),
(22,	'content/view/full/45',	0,	1,	1,	0,	'aad93975f09371695ba08292fd9698db',	'users/anonymous_users/anonymous_user'),
(28,	'content/view/full/51',	0,	1,	1,	0,	'38985339d4a5aadfc41ab292b4527046',	'media/images'),
(29,	'content/view/full/52',	0,	1,	1,	0,	'ad5a8c6f6aac3b1b9df267fe22e7aef6',	'media/files'),
(30,	'content/view/full/53',	0,	1,	1,	0,	'562a0ac498571c6c3529173184a2657c',	'media/multimedia');

DROP TABLE IF EXISTS `ezurlalias_ml`;
CREATE TABLE `ezurlalias_ml` (
  `parent` int(11) NOT NULL DEFAULT 0,
  `text_md5` varchar(32) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `action` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `action_type` varchar(32) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `alias_redirects` int(11) NOT NULL DEFAULT 1,
  `id` int(11) NOT NULL DEFAULT 0,
  `is_alias` int(11) NOT NULL DEFAULT 0,
  `is_original` int(11) NOT NULL DEFAULT 0,
  `lang_mask` bigint(20) NOT NULL DEFAULT 0,
  `link` int(11) NOT NULL DEFAULT 0,
  `text` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  PRIMARY KEY (`parent`,`text_md5`),
  KEY `ezurlalias_ml_actt_org_al` (`action_type`,`is_original`,`is_alias`),
  KEY `ezurlalias_ml_text_lang` (`text`(32),`lang_mask`,`parent`),
  KEY `ezurlalias_ml_par_act_id_lnk` (`action`(32),`id`,`link`,`parent`),
  KEY `ezurlalias_ml_par_lnk_txt` (`parent`,`text`(32),`link`),
  KEY `ezurlalias_ml_act_org` (`action`(32),`is_original`),
  KEY `ezurlalias_ml_text` (`text`(32),`id`,`link`),
  KEY `ezurlalias_ml_id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

INSERT INTO `ezurlalias_ml` (`parent`, `text_md5`, `action`, `action_type`, `alias_redirects`, `id`, `is_alias`, `is_original`, `lang_mask`, `link`, `text`) VALUES
(0,	'25d9c27d68ba0e40468e05a61f96d31d',	'eznode:56',	'eznode',	0,	41,	0,	1,	3,	41,	'site-skeletons'),
(0,	'50e2736330de124f6edea9b008556fe6',	'nop:',	'nop',	1,	17,	0,	0,	1,	17,	'media2'),
(0,	'62933a2951ef01f4eafd9bdf4d3cd2f0',	'eznode:43',	'eznode',	1,	9,	0,	1,	3,	9,	'Media'),
(0,	'76e323bf7efc1fad8935eb37bd557b92',	'eznode:42',	'eznode',	0,	40,	0,	1,	3,	40,	'ez-platform'),
(0,	'86425c35a33507d479f71ade53a669aa',	'nop:',	'nop',	1,	3,	0,	0,	1,	3,	'users2'),
(0,	'9bc65c2abec141778ffaa729489f3e87',	'eznode:5',	'eznode',	1,	2,	0,	1,	3,	2,	'Users'),
(0,	'ac68b62abfd6a9fe26e8ac4236c8ce0c',	'eznode:55',	'eznode',	0,	39,	0,	1,	2,	39,	'forms'),
(0,	'd41d8cd98f00b204e9800998ecf8427e',	'eznode:2',	'eznode',	1,	1,	0,	1,	3,	1,	''),
(2,	'a147e136bfa717592f2bd70bd4b53b17',	'eznode:14',	'eznode',	1,	6,	0,	1,	3,	6,	'Editors'),
(2,	'c2803c3fa1b0b5423237b4e018cae755',	'eznode:44',	'eznode',	1,	10,	0,	1,	3,	10,	'Anonymous-Users'),
(2,	'e57843d836e3af8ab611fde9e2139b3a',	'eznode:12',	'eznode',	1,	4,	0,	1,	3,	4,	'Guest-accounts'),
(2,	'f89fad7f8a3abc8c09e1deb46a420007',	'eznode:13',	'eznode',	1,	5,	0,	1,	3,	5,	'Administrator-users'),
(3,	'505e93077a6dde9034ad97a14ab022b1',	'nop:',	'nop',	1,	11,	0,	0,	1,	11,	'anonymous_users2'),
(3,	'70bb992820e73638731aa8de79b3329e',	'eznode:12',	'eznode',	1,	26,	0,	0,	1,	4,	'guest_accounts'),
(3,	'a147e136bfa717592f2bd70bd4b53b17',	'eznode:14',	'eznode',	1,	29,	0,	0,	1,	6,	'editors'),
(3,	'a7da338c20bf65f9f789c87296379c2a',	'nop:',	'nop',	1,	7,	0,	0,	1,	7,	'administrator_users2'),
(3,	'aeb8609aa933b0899aa012c71139c58c',	'eznode:13',	'eznode',	1,	27,	0,	0,	1,	5,	'administrator_users'),
(3,	'e9e5ad0c05ee1a43715572e5cc545926',	'eznode:44',	'eznode',	1,	30,	0,	0,	1,	10,	'anonymous_users'),
(5,	'5a9d7b0ec93173ef4fedee023209cb61',	'eznode:15',	'eznode',	1,	8,	0,	1,	3,	8,	'Administrator-User'),
(7,	'a3cca2de936df1e2f805710399989971',	'eznode:15',	'eznode',	1,	28,	0,	0,	0,	8,	'administrator_user'),
(9,	'2e5bc8831f7ae6a29530e7f1bbf2de9c',	'eznode:53',	'eznode',	1,	20,	0,	1,	3,	20,	'Multimedia'),
(9,	'45b963397aa40d4a0063e0d85e4fe7a1',	'eznode:52',	'eznode',	1,	19,	0,	1,	3,	19,	'Files'),
(9,	'59b514174bffe4ae402b3d63aad79fe0',	'eznode:51',	'eznode',	1,	18,	0,	1,	3,	18,	'Images'),
(10,	'ccb62ebca03a31272430bc414bd5cd5b',	'eznode:45',	'eznode',	1,	12,	0,	1,	3,	12,	'Anonymous-User'),
(11,	'c593ec85293ecb0e02d50d4c5c6c20eb',	'eznode:45',	'eznode',	1,	31,	0,	0,	1,	12,	'anonymous_user'),
(17,	'2e5bc8831f7ae6a29530e7f1bbf2de9c',	'eznode:53',	'eznode',	1,	34,	0,	0,	1,	20,	'multimedia'),
(17,	'45b963397aa40d4a0063e0d85e4fe7a1',	'eznode:52',	'eznode',	1,	33,	0,	0,	1,	19,	'files'),
(17,	'59b514174bffe4ae402b3d63aad79fe0',	'eznode:51',	'eznode',	1,	32,	0,	0,	1,	18,	'images'),
(19,	'2c5f0c4eb6b8ba8d176b87665bdbe1af',	'eznode:54',	'eznode',	0,	38,	0,	1,	3,	38,	'form-uploads');

DROP TABLE IF EXISTS `ezurlalias_ml_incr`;
CREATE TABLE `ezurlalias_ml_incr` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

INSERT INTO `ezurlalias_ml_incr` (`id`) VALUES
(1),
(2),
(3),
(4),
(5),
(6),
(7),
(8),
(9),
(10),
(11),
(12),
(13),
(14),
(15),
(16),
(17),
(18),
(19),
(20),
(21),
(22),
(24),
(25),
(26),
(27),
(28),
(29),
(30),
(31),
(32),
(33),
(34),
(35),
(36),
(37),
(38),
(39),
(40),
(41);

DROP TABLE IF EXISTS `ezurlwildcard`;
CREATE TABLE `ezurlwildcard` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `destination_url` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `source_url` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `type` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


DROP TABLE IF EXISTS `ezurl_object_link`;
CREATE TABLE `ezurl_object_link` (
  `contentobject_attribute_id` int(11) NOT NULL DEFAULT 0,
  `contentobject_attribute_version` int(11) NOT NULL DEFAULT 0,
  `url_id` int(11) NOT NULL DEFAULT 0,
  KEY `ezurl_ol_coa_id` (`contentobject_attribute_id`),
  KEY `ezurl_ol_url_id` (`url_id`),
  KEY `ezurl_ol_coa_version` (`contentobject_attribute_version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


DROP TABLE IF EXISTS `ezuser`;
CREATE TABLE `ezuser` (
  `contentobject_id` int(11) NOT NULL DEFAULT 0,
  `email` varchar(150) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `login` varchar(150) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `password_hash` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `password_hash_type` int(11) NOT NULL DEFAULT 1,
  `password_updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`contentobject_id`),
  UNIQUE KEY `ezuser_login` (`login`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

INSERT INTO `ezuser` (`contentobject_id`, `email`, `login`, `password_hash`, `password_hash_type`, `password_updated_at`) VALUES
(10,	'anonymous@link.invalid',	'anonymous',	'$2y$10$35gOSQs6JK4u4whyERaeUuVeQBi2TUBIZIfP7HEj7sfz.MxvTuOeC',	7,	NULL),
(14,	'admin@link.invalid',	'admin',	'$2y$10$FDn9NPwzhq85cLLxfD5Wu.L3SL3Z/LNCvhkltJUV0wcJj7ciJg2oy',	7,	NULL);

DROP TABLE IF EXISTS `ezuser_accountkey`;
CREATE TABLE `ezuser_accountkey` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hash_key` varchar(32) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `time` int(11) NOT NULL DEFAULT 0,
  `user_id` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `hash_key` (`hash_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


DROP TABLE IF EXISTS `ezuser_role`;
CREATE TABLE `ezuser_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contentobject_id` int(11) DEFAULT NULL,
  `limit_identifier` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT '',
  `limit_value` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT '',
  `role_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ezuser_role_role_id` (`role_id`),
  KEY `ezuser_role_contentobject_id` (`contentobject_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

INSERT INTO `ezuser_role` (`id`, `contentobject_id`, `limit_identifier`, `limit_value`, `role_id`) VALUES
(28,	11,	'',	'',	1),
(31,	42,	'',	'',	1),
(32,	13,	'Subtree',	'/1/2/',	3),
(33,	13,	'Subtree',	'/1/43/',	3),
(34,	12,	'',	'',	2),
(35,	13,	'',	'',	4);

DROP TABLE IF EXISTS `ezuser_setting`;
CREATE TABLE `ezuser_setting` (
  `user_id` int(11) NOT NULL DEFAULT 0,
  `is_enabled` int(11) NOT NULL DEFAULT 0,
  `max_login` int(11) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

INSERT INTO `ezuser_setting` (`user_id`, `is_enabled`, `max_login`) VALUES
(10,	1,	1000),
(14,	1,	10);

DROP TABLE IF EXISTS `ibexa_migrations`;
CREATE TABLE `ibexa_migrations` (
  `name` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `executed_at` datetime DEFAULT NULL,
  `execution_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


DROP TABLE IF EXISTS `ibexa_segments`;
CREATE TABLE `ibexa_segments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identifier` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`,`identifier`),
  UNIQUE KEY `ibexa_segments_identifier` (`identifier`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


DROP TABLE IF EXISTS `ibexa_segment_groups`;
CREATE TABLE `ibexa_segment_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identifier` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`,`identifier`),
  UNIQUE KEY `ibexa_segment_groups_identifier` (`identifier`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


DROP TABLE IF EXISTS `ibexa_segment_group_map`;
CREATE TABLE `ibexa_segment_group_map` (
  `segment_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`segment_id`,`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


DROP TABLE IF EXISTS `ibexa_segment_user_map`;
CREATE TABLE `ibexa_segment_user_map` (
  `segment_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`segment_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


DROP TABLE IF EXISTS `ibexa_setting`;
CREATE TABLE `ibexa_setting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group` varchar(128) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `identifier` varchar(128) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `value` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL COMMENT '(DC2Type:json)',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ibexa_setting_group_identifier` (`group`,`identifier`),
  KEY `ibexa_setting_id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

INSERT INTO `ibexa_setting` (`id`, `group`, `identifier`, `value`) VALUES
(1,	'personalization',	'installation_key',	'\"\"');

-- 2021-06-03 02:38:28
