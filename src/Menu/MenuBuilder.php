<?php

namespace App\Menu;

use Knp\Menu\FactoryInterface;

class MenuBuilder
{
    private $factory;

    public function __construct(FactoryInterface $factory)
    {
        $this->factory = $factory;
    }

    public function buildMenu()
    {
        $menu = $this->factory->createItem('root')
            ->setChildrenAttribute('class', 'navbar-nav ms-auto mb-2 mb-lg-0');

        $menu->addChild('Home', ['route' => 'ez_urlalias', 'routeParameters' => [
            'locationId' => 91,
        ]])
            ->setAttribute('class', 'nav-item')
            ->setLinkAttribute('class', 'nav-link active');

        $menu->addChild('Nuestros paquetes', ['route' => 'ez_urlalias', 'routeParameters' => [
            'locationId' => 93,
        ]])
            ->setAttribute('class', 'nav-item')
            ->setLinkAttribute('class', 'nav-link');

        $menu->addChild('Alojamientos', ['route' => 'ez_urlalias', 'routeParameters' => [
            'locationId' => 93,
        ]])
            ->setAttribute('class', 'nav-item')
            ->setLinkAttribute('class', 'nav-link');
        
        $menu->addChild('Vuelos', ['route' => 'ez_urlalias', 'routeParameters' => [
            'locationId' => 93,
        ]])
            ->setAttribute('class', 'nav-item')
            ->setLinkAttribute('class', 'nav-link');

        $menu->addChild('Guía turístico', ['route' => 'ez_urlalias', 'routeParameters' => [
            'locationId' => 93,
        ]])
            ->setAttribute('class', 'nav-item')
            ->setLinkAttribute('class', 'nav-link');
            
        $menu->addChild('Contáctenos', ['route' => 'ez_urlalias', 'routeParameters' => [
            'locationId' => 93,
        ]])
            ->setAttribute('class', 'nav-item')
            ->setLinkAttribute('class', 'nav-link');
            
        $menu->addChild('Acerca de nosotros', ['route' => 'ez_urlalias', 'routeParameters' => [
            'locationId' => 93,
        ]])
            ->setAttribute('class', 'nav-item')
            ->setLinkAttribute('class', 'nav-link');

        return $menu;
    }
}