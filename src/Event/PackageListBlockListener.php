<?php

declare(strict_types=1);

namespace App\Event;

use EzSystems\EzPlatformPageFieldType\FieldType\Page\Block\Renderer\BlockRenderEvents;
use EzSystems\EzPlatformPageFieldType\FieldType\Page\Block\Renderer\Event\PreRenderEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use App\TravelBoxBundle\Services\ContentService;

class PackageListBlockListener implements EventSubscriberInterface
{
    /** @var \App\TravelBoxBundle\Services\ContentService */
    private $contentService;

    /**
     * @param \App\TravelBoxBundle\Services\ContentService $contentService
     */
    public function __construct(
        ContentService $contentService
    ) {
        $this->contentService = $contentService;
    }

    /**
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents()
    {
        return [
            BlockRenderEvents::getBlockPreRenderEventName('list_tourist_package') => 'onBlockPreRender',
        ];
    }

    /**
     * @param \EzSystems\EzPlatformPageFieldType\FieldType\Page\Block\Renderer\Event\PreRenderEvent $event
     *
     * @throws \eZ\Publish\API\Repository\Exceptions\InvalidArgumentException
     * @throws \eZ\Publish\API\Repository\Exceptions\NotFoundException
     * @throws \eZ\Publish\API\Repository\Exceptions\UnauthorizedException
     */
    public function onBlockPreRender(PreRenderEvent $event): void
    {
        $blockValue = $event->getBlockValue();
        $renderRequest = $event->getRenderRequest();

        $parameters = $renderRequest->getParameters();

        $contentIdAttribute = $blockValue->getAttribute('list_tourist_package_contents')->getValue();

        $parameters['list_tourist_package'] = array_map(
            fn ($id) => $this->contentService->getContentByLocationId((int) intval($id)),
            explode(',', $contentIdAttribute) 
        );

        foreach($parameters['list_tourist_package'] as $content){
            if($content->getField('image')){
                $image_id = $content->getField('image')->value->destinationContentIds[0];
                $location = $this->contentService->loadLocationByContentId($image_id);
                $image = $this->contentService->getContentByLocationId($location->id);
                $parameters['list_tourist_package_images'][$image_id] = $image;
            }
        }

        $renderRequest->setParameters($parameters);
    }
}