<?php

declare(strict_types=1);

namespace App\Event;

use EzSystems\EzPlatformPageFieldType\FieldType\Page\Block\Renderer\BlockRenderEvents;
use EzSystems\EzPlatformPageFieldType\FieldType\Page\Block\Renderer\Event\PreRenderEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use App\TravelBoxBundle\Services\ContentService;

class TourGuideListBlockListener implements EventSubscriberInterface
{
    /** @var \App\TravelBoxBundle\Services\ContentService */
    private $contentService;

    /**
     * @param \App\TravelBoxBundle\Services\ContentService $contentService
     */
    public function __construct(
        ContentService $contentService
    ) {
        $this->contentService = $contentService;
    }

    /**
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents()
    {
        return [
            BlockRenderEvents::getBlockPreRenderEventName('tourist_guides_list') => 'onBlockPreRender',
        ];
    }

    /**
     * @param \EzSystems\EzPlatformPageFieldType\FieldType\Page\Block\Renderer\Event\PreRenderEvent $event
     *
     * @throws \eZ\Publish\API\Repository\Exceptions\InvalidArgumentException
     * @throws \eZ\Publish\API\Repository\Exceptions\NotFoundException
     * @throws \eZ\Publish\API\Repository\Exceptions\UnauthorizedException
     */
    public function onBlockPreRender(PreRenderEvent $event): void
    {
        $blockValue = $event->getBlockValue();
        $renderRequest = $event->getRenderRequest();

        $parameters = $renderRequest->getParameters();

        $contentIdAttribute = $blockValue->getAttribute('tourist_guides_contents')->getValue();
        
        $parameters['tourist_guides_content_pro'] = array_map(
            fn ($id) => $this->contentService->getContentByLocationId((int) intval($id)),
            explode(',', $contentIdAttribute)
        );

        $renderRequest->setParameters($parameters);
    }

}