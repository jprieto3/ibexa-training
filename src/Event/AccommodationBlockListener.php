<?php

declare(strict_types=1);

namespace App\Event;

use EzSystems\EzPlatformPageFieldType\FieldType\Page\Block\Renderer\BlockRenderEvents;
use EzSystems\EzPlatformPageFieldType\FieldType\Page\Block\Renderer\Event\PreRenderEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use App\TravelBoxBundle\Services\ContentService;

class AccommodationBlockListener implements EventSubscriberInterface
{
    /** @var \App\TravelBoxBundle\Services\ContentService */
    private $contentService;

    /**
     * @param \App\TravelBoxBundle\Services\ContentService $contentService
     */
    public function __construct(
        ContentService $contentService
    ) {
        $this->contentService = $contentService;
    }

    /**
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents()
    {
        return [
            BlockRenderEvents::getBlockPreRenderEventName('accommodation_list') => 'onBlockPreRender',
        ];
    }

    /**
     * @param \EzSystems\EzPlatformPageFieldType\FieldType\Page\Block\Renderer\Event\PreRenderEvent $event
     *
     * @throws \eZ\Publish\API\Repository\Exceptions\InvalidArgumentException
     * @throws \eZ\Publish\API\Repository\Exceptions\NotFoundException
     * @throws \eZ\Publish\API\Repository\Exceptions\UnauthorizedException
     */
    public function onBlockPreRender(PreRenderEvent $event): void
    {
        $blockValue = $event->getBlockValue();
        $renderRequest = $event->getRenderRequest();

        $parameters = $renderRequest->getParameters();

        $contentIdAttribute = $blockValue->getAttribute('accommodation_list_contents')->getValue();

        $parameters['accommodations_list'] = array_map(
            fn ($id) => $this->contentService->getContentByLocationId((int) intval($id)),
            explode(',', $contentIdAttribute) 
        );

        foreach($parameters['accommodations_list'] as $content){
            $image_id = $content->getField('accommodation_images')->value->destinationContentIds[0];
            $location = $this->contentService->loadLocationByContentId($image_id);
            $image = $this->contentService->getContentByLocationId($location->id);
            $parameters['accommodations_list_images'][$image_id] = $image;
        }

        $renderRequest->setParameters($parameters);
    }
}