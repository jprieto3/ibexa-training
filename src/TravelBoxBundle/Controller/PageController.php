<?php

namespace App\TravelBoxBundle\Controller;

use eZ\Publish\Core\MVC\ConfigResolverInterface;
use eZ\Bundle\EzPublishCoreBundle\Controller;
use App\TravelBoxBundle\Services\ContentService;
use Psr\Log\LoggerInterface;

class PageController extends Controller
{
    private $configResolver;
    private ContentService $contentService;
    private LoggerInterface $logger;
 
    public function __construct( 
        ContentService $contentService,
        ConfigResolverInterface $configResolver,
        LoggerInterface $logger 
        )
    {
        $this->contentService = $contentService;
        $this->configResolver = $configResolver;
        $this->logger = $logger;
    }

    public function getHeaderAction()
    {
        try {
            $configValue = $this->configResolver->getParameter('header', 'travelbox');
            $content = $this->contentService->getContentByLocationId($configValue['locationId']);
            $menu = $this->contentService->getRelationsContent($content, 'menu');

            return $this->render(
                '@TravelBox/themes/travelbox/header.html.twig',
                array(
                    'content' => $content,
                    'menu' => $menu
                )
            );
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage().' in file '.$e->getFile().' in line '.$e->getLine());
            return $e;
        }
    }

    public function getFooterAction()
    {
        try {
            $configValue = $this->configResolver->getParameter('footer', 'travelbox');
            $content = $this->contentService->getContentByLocationId($configValue['locationId']);
            $menu = $this->contentService->getRelationsContent($content, 'menu');

            return $this->render(
                '@TravelBox/themes/travelbox/footer.html.twig',
                array(
                    'content' => $content,
                    'menu' => $menu
                )
            );
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage().' in file '.$e->getFile().' in line '.$e->getLine());
            return $e;
        }
    }
}