# Initial variables
os ?= $(shell uname -s)

# Load custom setitngs
-include .env
export

PROVISION ?= docker
include etc/$(PROVISION)/makefile

DEPLOYMENT ?= ecs
include etc/$(DEPLOYMENT)/makefile


install i: build up
postinstall pi: permission index images.clear.cache

squash: branch ?= $(shell git rev-parse --abbrev-ref HEAD)
squash:
	git rebase -i $(shell git merge-base origin/$(branch) origin/master)
	git push -f

git g: branch ?= $(shell git rev-parse --abbrev-ref HEAD)
git g: ## Review, add, commit and push changes using commitizen. Usage: make push
	git diff
	git add -A .
	@docker run --rm -it -v $(CURDIR):/app -v $(HOME)/.gitconfig:/root/.gitconfig aplyca/commitizen
	git pull origin $(branch) ||: true
	git push -u origin $(branch)

test: #behat codecept ## Run all tests.
	curl -I localhost/login | grep -e "200 OK"
	
h help: ## ℹ️  This help.
	@echo 'ℹ️  Usage: make <task> [option=value]' 
	@echo 'Default task: init'
	@echo
	@echo '🛠️  Tasks:'
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z0-9., _-]+:.*?## / {printf "  \033[36m%-15s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

.PHONY: all test $(MAKECMDGOALS)

permission:
	sudo chmod -R 777 ./etc/elasticsearch && sudo chmod -R 777 ./etc/minio

.DEFAULT_GOAL := up

#### OK

# ifdef command
# override command := -c "$(command)"
# endif

# install i: build up
# postinstall pi: index images.clear.cache

# ps:
# 	@docker-compose --env-file .env.local ps

# up:
# 	@docker-compose --env-file .env.local up -d --remove-orphans

# down:
# 	@docker-compose --env-file .env.local down

# build: service ?= app
# build:
# 	@docker-compose --env-file .env.local build $(service)

# stop:
# 	@docker-compose --env-file .env.local stop

# start:
# 	@docker-compose --env-file .env.local start

# logs l: service ?= app
# logs l:
# 	@docker-compose --env-file .env.local logs -f $(service)

# exec cli: service ?= app
# exec cli: bash ?= ash
# exec cli:
# 	@docker-compose --env-file .env.local exec $(service) $(bash) $(command)

# reload:
# 	@make stop && make up

# copy: service ?= app
# copy: path ?= /app/composer.lock
# copy: ## Copy app files/directories from service container to host
# 	@docker cp $(shell docker-compose --env-file ./.env.local ps -q $(service)):$(path) .

# ## DB Tasks
# dumpdb: filename ?= `date +'db_backup%d%m%Y.sql'`
# dumpdb:
# 	@make exec service=db bash=bash command="mysqldump -u root --add-drop-table ezplatform > $(filename) -p"

# sync.db sd: filename ?= `date +'db_backup%d%m%Y.sql'`
# sync.db sd:
# 	@make copy service=db path=/$(filename)

# ## Index the elasticsearch
# index:
# 	@make exec command="php bin/console ibexa:elasticsearch:put-index-template --overwrite && php bin/console ibexa:reindex"

# ## Clear cache image aliases
# images.clear.cache icc:
# 	@make exec command="php bin/console liip:imagine:cache:remove"

# .DEFAULT_GOAL := up
