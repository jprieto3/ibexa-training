stages:
    - Build
    - Deploy
    - Tasks

.changes: &changes
  - config/**/*
  - src/**/*
  - templates/**/*
  - public/index.php
  - composer.json
  - composer.lock                  
  - etc/docker/app/*  
  - .gitlab-ci.yml

.web-changes: &web-changes
  - etc/docker/nginx/*

.all-changes: &all-changes
  - config/**/*
  - src/**/*
  - templates/**/*
  - public/index.php
  - composer.json
  - composer.lock                  
  - etc/docker/app/*  
  - .gitlab-ci.yml
  - etc/docker/nginx/*

variables:
  GITLAB_TOKEN: ${GITLAB_ACCESS_TOKEN}
  DOCKER_HOST: tcp://docker:2375/
  # OPS_HOSTNAME: axp.aplyca.com
  PUBLIC_HOSTNAME: cms.axp.aplyca.com
  APP_NAME: axp/cms

.Create image:
  stage: Build
  image:
    name: docker/compose:1.24.1
    entrypoint: ["/bin/sh", "-c"]
  cache: {}
  variables:
    GIT_DEPTH: 1
    TARGET_IMAGE: prod
    IMAGE_TAG: ":$CI_COMMIT_REF_NAME"
    SERVICE: app
    IMAGE_REPO: $REGISTRY_HOSTNAME/$APP_NAME/$SERVICE
  before_script:
    - apk add --quiet make
    - apk add --quiet git
    - make login.ecr
  script:
    - IMAGE_CACHE_TAG=:latest IMAGE_TAG=:master make pull service=$SERVICE
    - make pull service=$SERVICE
    - make build service=$SERVICE
    - make push service=$SERVICE
  environment:
    name: "App images/$CI_COMMIT_REF_NAME"
    url: "https://$REGISTRY_HOSTNAME/$APP_NAME/$SERVICE:$CI_COMMIT_REF_NAME"
    on_stop: Delete App image
  services:
    - docker:stable-dind

UAT App image:
  extends: .Create image
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
      changes: *changes
    - if: '$CI_COMMIT_REF_NAME == "master"'
      when: never
    - if: '$CI_COMMIT_TAG != null'
      when: never

UAT Web image:
  extends: .Create image
  variables:
    SERVICE: server
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
      changes: *web-changes
    - if: '$CI_COMMIT_REF_NAME == "master"'
      when: never
    - if: '$CI_COMMIT_TAG != null'
      when: never
  environment:
    name: "Web images/$CI_COMMIT_REF_NAME"
    on_stop: Delete Web image

Delete App image:
  stage: Tasks
  image:
    name: infrastructureascode/aws-cli
  cache: {}        
  variables:
    SERVICE: app
  before_script:
    - apk add --quiet --update make        
  script:
    - make remove.image repo=$APP_NAME/$SERVICE version=$CI_COMMIT_REF_NAME     
  environment:
    name: "App images/$CI_COMMIT_REF_NAME"
    action: stop
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
      changes: *changes
      when: manual
    - if: '$CI_COMMIT_REF_NAME == "master"'
      when: never
    - if: '$CI_COMMIT_TAG != null'
      when: never     
  needs: ["UAT App image"]

Delete Web image:
  extends: Delete App image
  variables:
    SERVICE: server
  environment:
    name: "Web images/$CI_COMMIT_REF_NAME"
    action: stop          
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
      changes: *web-changes
      when: manual
    - if: '$CI_COMMIT_REF_NAME == "master"'
      when: never
    - if: '$CI_COMMIT_TAG != null'
      when: never     
  needs: ["UAT Web image"]

To Review in QA:
  stage: Deploy
  image:
    name: silintl/ecs-deploy
    entrypoint: ["/bin/sh", "-c"]
  cache: {}
  variables:
    GIT_DEPTH: 1
    # SERVICE: app
    # IMAGE_REPO: $REGISTRY_HOSTNAME/$APP_NAME/$SERVICE
    ENVIRONMENT: Prod
    CLUSTER: AXP-PROD
  before_script:
    - apk add --quiet --update make git
    - apk add --quiet curl    
    - ln -s /ecs-deploy /usr/bin/ecs-deploy
  script:
    - make deploy.service.multitask cluster=${CLUSTER} environment=${ENVIRONMENT} version=$CI_COMMIT_REF_NAME service=AXP-CMS-Prod
  environment:
    name: QA
    url: "https://PUBLIC_HOSTNAME"
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
      changes: *all-changes
      when: manual
    - if: '$CI_COMMIT_REF_NAME == "master"'
      when: never
    - if: '$CI_COMMIT_TAG != null'
      when: never 
  needs: ["UAT App image"]

Stable App image:
  extends: UAT App image
  variables:
    IMAGE_CACHE_TAG: ":latest"
  rules:
    - if: '$CI_COMMIT_REF_NAME == "master"'
      changes: *changes
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
      when: never
    - if: '$CI_COMMIT_TAG != null'
      when: never 

Stable Web image:
  extends: Stable App image
  variables:
    SERVICE: server
  rules:
    - if: '$CI_COMMIT_REF_NAME == "master"'
      changes: *web-changes
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
      when: never
    - if: '$CI_COMMIT_TAG != null'
      when: never 
  environment:
    name: "Web images/$CI_COMMIT_REF_NAME"
    on_stop: Delete Web image

To QA:
  stage: Deploy
  image:
    name: silintl/ecs-deploy
    entrypoint: ["/bin/sh", "-c"]
  cache: {}
  variables:
    GIT_DEPTH: 1
    # SERVICE: app
    # IMAGE_REPO: $REGISTRY_HOSTNAME/$APP_NAME/$SERVICE
    ENVIRONMENT: Prod
    CLUSTER: AXP-PROD        
  before_script:
    - apk add --quiet --update make git
    - apk add --quiet curl    
    - ln -s /ecs-deploy /usr/bin/ecs-deploy
  script:
    - make deploy.service.multitask cluster=${CLUSTER} environment=${ENVIRONMENT} version=$CI_COMMIT_REF_NAME service=AXP-CMS-Prod
  environment:
    name: QA
    url: "https://PUBLIC_HOSTNAME"
  rules:
    - if: '$CI_COMMIT_REF_NAME == "master"'
      changes: *all-changes
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
      when: never
    - if: '$CI_COMMIT_TAG != null'
      when: never
  needs: ["Stable App image"]

  # Test build 00